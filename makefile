CLI_DIR=cmd/commandLine
GUI_DIR=cmd/gui
CERT_DIR=certs
CERT=$(CERT_DIR)/server.crt
GODOT=godot
GO=go
CLI_OUTPUT=bin/phantom_envoy_cli
GUI_OUTPUT=bin/phantom_envoy_gui

all: $(CERT_DIR) $(CERT) bin $(CLI_OUTPUT) $(GUI_OUTPUT)

$(CLI_OUTPUT):
	$(GO) get -v "github.com/c-bata/go-prompt"
	$(GO) get -v "gitlab.com/kohlten/protocol/client"
	$(GO) get -v "gitlab.com/kohlten/protocol/events"
	$(GO) get -v "gitlab.com/kohlten/protocol/protocol"
	$(GO) get -v "gitlab.com/kohlten/protocol/server"
	$(GO) get -v "gitlab.com/kohlten/protocol/util"
	$(GO) get -v github.com/google/uuid
	cd $(CLI_DIR) && go build -v
	mv $(CLI_DIR)/commandLine bin/phantom_envoy_cli

$(CERT_DIR):
	mkdir $(CERT_DIR)

$(CERT):
	openssl ecparam -genkey -name secp384r1 -out $(CERT_DIR)/server.key
	openssl req -new -x509 -sha256 -key $(CERT_DIR)/server.key -out $(CERT_DIR)/server.crt -days 3650 -subj "/C=US/ST=*/L=*/O=PhantomEnvoy"

$(GUI_OUTPUT):
	$(GO) get -v github.com/go-gl/gl/v3.2-core/gl
	$(GO) get -v github.com/veandco/go-sdl2/sdl
	$(GO) get -v gitlab.com/kohlten/glLib
	$(GO) get -v github.com/inkyblackness/imgui-go
	$(GO) get -v gitlab.com/kohlten/fileSelector
	$(GO) get -v gitlab.com/kohlten/gamelib
	cd $(GUI_DIR) && go build
	mv $(GUI_DIR)/gui bin/phantom_envoy_gui

bin:
	mkdir bin

re: clean all

clean:
	rm -rf bin
	rm -rf $(CERT_DIR)

.PHONY: gui clean all re
