package main

import (
	"errors"
	"fmt"
	"gitlab.com/kohlten/phantomenvoy/internal/events"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
	"strings"
)

type CommandFunc func(args []string) error

var Commands = map[string]CommandFunc{
	"start_listener":          StartListener,
	"strl":                    StartListener,
	"stop_listener":           StopListener,
	"stpl":                    StopListener,
	"search":                  Search,
	"s":                       Search,
	"available_clients":       AvailableClients,
	"ac":                      AvailableClients,
	"reset_available_clients": ResetAvailableClients,
	"rac":                     ResetAvailableClients,
	"start_sender":            StartSender,
	"strs":                    StartSender,
	"stop_sender":             StopSender,
	"stps":                    StopSender,
	"start_receiver":          StartReceiver,
	"strr":                    StartReceiver,
	"stop_receiver":           StopReceiver,
	"stpr":                    StopReceiver,
	"send_data":               SendData,
	"sd":                      SendData,
	"send_file":               SendFile,
	"sf":                      SendFile,
	"poll":                    Poll,
	"p":                       Poll,
	"request_response":        RequestResponse,
	"rr":                      RequestResponse,
	"get_data":                GetData,
	"gd":                      GetData,
	"get_file":                GetFile,
	"gf":                      GetFile,
	"processing_file_info":    ProcessingFileInfo,
	"pfi":                     ProcessingFileInfo,
	"received_file_info":      ReceivedFileInfo,
	"rfi":                     ReceivedFileInfo,
	"cancel":                  Cancel,
	"c":                       Cancel,
	"help":                    Help,
	"h":                       Help,
	"quit":                    Quit,
	"q":                       Quit,
}

var CommandHelp = map[string]string{
	"start_listener":          "Starts the listening module. By doing this you can run search and available clients.",
	"strl":                    "Shorthand for start_listener.",
	"stop_listener":           "Stops the listening module.",
	"stpl":                    "Shorthand for stop_listener.",
	"search":                  "Searches for clients on the local network. The normal use case is to start the searcher, then search, the see the clients by running available_clients.",
	"s":                       "Shorthand for search.",
	"available_clients":       "Prints out all of the available clients.",
	"ac":                      "Shorthand for available_clients.",
	"reset_available_clients": "Resets the list of available clients.",
	"rac":                     "Shorthand for reset_available_clients.",
	"start_sender":            "Starts the sending module.",
	"strs":                    "Shorthand for start_sender.",
	"stop_sender":             "Stops the sending module.",
	"stps":                    "Shorthand for stop_sender.",
	"start_receiver":          "Starts the receiving module.",
	"strr":                    "Shorthand for start_receiver.",
	"stop_receiver":           "Stops the receiving module.",
	"stpr":                    "Shorthand for stop_receiver.",
	"send_data":               "Sends data to an ip. The arguments for this command are the ip you want to send to and the data you want to send it.",
	"sd":                      "Shorthand for send_data.",
	"send_file":               "Sends a file to an ip. The arguments for this command are the ip you want to send to and the filename of the file you want to send.",
	"sf":                      "Shorthand for send_file",
	"poll":                    "Polls for events going on. This allows you to see the current state of everything and if there is any errors. It will also show you if someone has requested to send a file to you.",
	"p":                       "Shorthand for poll.",
	"request_response":        "Send a response to someone wanting to send you a file. The arguments to this command are the id of the file and the response, either true or false.",
	"rr":                      "Shorthand for request_response.",
	"get_data":                "Get the data that someone has sent you and print it out. The arguments to this command are the id of the file.",
	"gd":                      "Shorthand for get_data.",
	"get_file":                "Get the data that someone has sent you and print it out. The arguments to this command are the id of the file and the directory to place the file. If the person sent data and not a file, the file will be named \"Data\".",
	"gf":                      "Shorthand for get_file.",
	"processing_file_info":    "Prints out the info of all processing files in the receiver module.",
	"pfi":                     "Shorthand for processing_file_info.",
	"received_file_info":      "Prints out the info of all received files in the receiver module.",
	"rfi":                     "Shorthand for received_file_info.",
	"cancel":                  "Cancels a file on the sender or receiver.",
	"c":                       "Shorthand for cancel.",
	"help":                    "Prints all of the commands and their help or if you provide a command name, it will print the help of that command.",
	"h":                       "Shorthand for help.",
	"quit":                    "Quits out of the ",
	"q":                       "Shorthand for quit",
}


func StartListener(args []string) error {
	return phantom.StartListener()
}

func StopListener(args []string) error {
	return phantom.StopListener()
}

func StartSender(args []string) error {
	return phantom.StartSender()
}

func StopSender(args []string) error {
	return phantom.StopSender()
}

func StartReceiver(args []string) error {
	return phantom.StartReceiver()
}

func StopReceiver(args []string) error {
	return phantom.StopReceiver()
}

func Search(args []string) error {
	return phantom.Search()
}

func AvailableClients(args []string) error {
	available, err := phantom.GetAddresses()
	if err != nil {
		return err
	}
	for i := 0; i < len(available); i++ {
		fmt.Println(available[i].Ip, available[i].Name)
	}
	return nil
}

func ResetAvailableClients(args []string) error {
	return phantom.ClearAddresses()
}

// Name is not currently supported
// args: address/name file
func SendFile(args []string) error {
	if len(args) != 2 {
		return errors.New("sendfile requires an address or name and a filename")
	}
	address := args[0]
	filename := args[1]
	return phantom.Send(address, filename)
}

// args: address/name data
func SendData(args []string) error {
	if len(args) < 2 {
		return errors.New("sendfile requires an address or name and a filename")
	}
	address := args[0]
	data := []byte(strings.Join(args[1:], " "))
	temp, err := ioutil.TempFile("", "")
	if err != nil {
		return err
	}
	_, err = temp.Write(data)
	if err != nil {
		return err
	}
	name := temp.Name()
	temp.Close()
	err = phantom.Send(address, name)
	_ = os.Remove(name)
	return err
}

//            bool
// args: id response
func RequestResponse(args []string) error {
	if len(args) != 2 {
		return errors.New("request_response requires an id and a response")
	}
	response, err := strconv.ParseBool(args[1])
	if err != nil {
		return err
	}
	phantom.Push(&events.RequestResult{
		Client:   0,
		Id:       args[0],
		Filename: "",
		Result:   response,
	})
	return nil
}

func Poll(args []string) error {
	for {
		event := phantom.Poll()
		if event == nil {
			break
		}
		switch e := event.(type) {
		case *events.RequestToSend:
			fmt.Println("Send Request\n    Filename:", e.Filename, "\n    Chunks:", e.Chunks, "\n    Size:", e.FileSize, "\n    Hash:", e.Hash, "\n    ID:", e.Id)
		case *events.Error:
			fmt.Println("Send Error:\n    Ip:", e.Ip, "\n    Reason:", e.Reason)
		case *events.SendResult:
			fmt.Println("Send Result\n    Filename:", e.Filename, "\n    ID:", e.Id, "\n    Result:", e.Result)
		}
	}
	return nil
}

// Args: ids of files to show. If none, will show all
func ProcessingFileInfo(args []string) error {
	files := append(phantom.GetProcessingFileInfo(), phantom.GetSendingFileInfo()...)
	if len(args) > 0 {
		for i := 0; i < len(args); i++ {
			for j := 0; j < len(files); j++ {
				if files[j].Id == args[i] {
					file := files[i]
					fmt.Println("Processing File Info for", file.Name, "\n    Chunks:", file.TotalChunks, "\n    Chunks Received:", file.Chunks, "\n    ID:", file.Id, "\n    Hash:", file.Hash, "\n    Size:", file.Size)
				}
			}
		}
	} else {
		for i := 0; i < len(files); i++ {
			found := false
			for j := 0; j < i; j++ {
				if files[j].Id == files[i].Id {
					found = true
				}
			}
			if !found {
				file := files[i]
				fmt.Println("Processing File Info for", file.Name, "\n    Chunks:", file.TotalChunks, "\n    Chunks Received:", file.Chunks, "\n    ID:", file.Id, "\n    Hash:", file.Hash, "\n    Size:", file.Size)
			}
		}
	}
	return nil
}

// Args: ids of files to show. If none, will show all
func ReceivedFileInfo(args []string) error {
	files := phantom.GetReceivedFileInfo()
	if len(args) > 0 {
		for i := 0; i < len(args); i++ {
			for j := 0; j < len(files); j++ {
				if files[j].Id == args[i] {
					file := files[i]
					fmt.Println("Received File Info for", file.Name, "\n    Chunks:", file.TotalChunks, "\n    Chunks Received:", file.Chunks, "\n    ID:", file.Id, "\n    Hash:", file.Hash, "\n    Size:", file.Size)
				}
			}
		}
	} else {
		for i := 0; i < len(files); i++ {
			file := files[i]
			fmt.Println("Received File Info for", file.Name, "\n    Chunks:", file.TotalChunks, "\n    Chunks Received:", file.Chunks, "\n    ID:", file.Id, "\n    Hash:", file.Hash, "\n    Size:", file.Size)
		}
	}
	return nil
}

// args: id
func GetData(args []string) error {
	if len(args) != 1 {
		return errors.New("get_data requires an id")
	}
	id := args[0]
	file, err := phantom.GetFile(id)
	if err != nil {
		return err
	}
	data := make([]byte, file.Size)
	fd, err := os.OpenFile(file.OutputName, os.O_RDWR, 0755)
	if err != nil {
		return err
	}
	_, _ = fd.Read(data)
	_ = fd.Close()
	_ = os.Remove(file.OutputName)
	fmt.Println("Data\n    Filename:", file.Name, "\n    Size:", file.Size, "\n    Hash:", file.Hash, "\n    Data:", string(data))
	return nil
}

// args: id outputDir
func GetFile(args []string) error {
	if len(args) != 2 {
		return errors.New("get_data requires an id and an output directory")
	}
	id := args[0]
	file, err := phantom.GetFile(id)
	outputDir := args[1]
	if err != nil {
		return err
	}
	if outputDir[len(outputDir)-1] != '/' {
		outputDir += "/"
	}
	err = os.Rename(file.OutputName, outputDir+file.Name)
	if err != nil {
		return err
	}
	return nil
}

// args: id
func Cancel(args []string) error {
	if len(args) != 1 {
		return errors.New("cancel requires an id")
	}
	phantom.Cancel(args[0])
	return nil
}

func Quit(args []string) error {
	return errors.New("quit")
}

// args: command
func Help(args []string) error {
	if len(args) > 0 {
		for i := 0; i < len(args); i++ {
			printCommandHelp(args[i])
		}
	} else {
		keys := make([]string, len(CommandHelp))
		i := 0
		for k, _ := range CommandHelp {
			keys[i] = k
		}
		sort.Strings(keys)
		for _, k := range keys {
			printCommandHelp(k)
		}
	}
	return nil
}

func printCommandHelp(command string) {
	commandHelp, ok := CommandHelp[command]
	if !ok {
		fmt.Println(command, "was not found.")
		return
	}
	split := strings.Split(commandHelp, ".")
	fmt.Println(fmt.Sprint(command, ":"))
	for i := 0; i < len(split); i++ {
		if len(split[i]) > 0 {
			if i > 0 && split[i][0] == ' ' {
				split[i] = split[i][1:]
			}
			fmt.Println(fmt.Sprint("    ", split[i], "."))
		}
	}
}
