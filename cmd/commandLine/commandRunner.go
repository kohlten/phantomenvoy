package main

import (
	"errors"
	"fmt"
	"strings"
)


func RunCommand(args []string) error {
	if len(args) == 0 {
		return nil
	}
	command := strings.ToLower(args[0])
	args = args[1:]
	commandFunc, ok := Commands[command]
	if !ok {
		return errors.New(fmt.Sprint("command ", command, " was not found"))
	}
	return commandFunc(args)
}
