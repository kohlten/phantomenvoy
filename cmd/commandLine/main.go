package main

import (
	"fmt"
	"github.com/c-bata/go-prompt"
	"gitlab.com/kohlten/phantomenvoy/internal"
	"gitlab.com/kohlten/phantomenvoy/internal/file"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

var suggestions []prompt.Suggest
var exit bool
var phantom *internal.Phantom

func setupSignals() {
	signals := make(chan os.Signal, 1)
	signal.Ignore(syscall.SIGWINCH)
	signal.Notify(signals)
	go func() {
		for {
			sig := <-signals
			switch sig {
			case syscall.SIGABRT:
				fallthrough
			case syscall.SIGKILL:
				fallthrough
			case syscall.SIGALRM:
				fallthrough
			case syscall.SIGINT:
				fallthrough
			case syscall.SIGPIPE:
				fallthrough
			case syscall.SIGQUIT:
				fallthrough
			case syscall.SIGSTOP:
				fallthrough
			case syscall.SIGTERM:
				fallthrough
			case syscall.SIGHUP:
				exit = true
				break
			}
		}
	}()
}

func main() {
	p, err := internal.NewPhantom()
	if err != nil {
		log.Fatal(err.Error())
	}
	phantom = p
	setupSignals()
	suggestions = mapToSuggestions()
	prompt.New(executor, completer, prompt.OptionPrefix(">>> "), prompt.OptionSetExitCheckerOnInput(checkExit)).Run()
	phantom.Close()
}

func split(line string) []string {
	line = strings.TrimSpace(line)
	return strings.Split(line, " ")
}

func executor(s string) {
	split := split(s)
	if len(split) > 0 {
		err := RunCommand(split)
		if err != nil {
			if err.Error() == "quit" {
				exit = true
			} else {
				fmt.Println("Error: ", err.Error())
			}
		}
	}
}

func fileCompleter(files []*file.Info) []prompt.Suggest {
	fileSuggestions := make([]prompt.Suggest, 0)
	for i := 0; i < len(files); i++ {
		found := false
		for j := 0; j < i; j++ {
			if files[i].Id == files[j].Id {
				found = true
				break
			}
		}
		if !found {
			suggestion := prompt.Suggest{Text: files[i].Id, Description: files[i].Name}
			fileSuggestions = append(fileSuggestions, suggestion)
		}
	}
	return fileSuggestions
}

func rfCompleter() []prompt.Suggest {
	files := phantom.GetReceivedFileInfo()
	return fileCompleter(files)
}

func pfCompleter() []prompt.Suggest {
	files := append(phantom.GetProcessingFileInfo(), phantom.GetSendingFileInfo()...)
	return fileCompleter(files)
}

func ipCompleter() []prompt.Suggest {
	ips, err := phantom.GetAddresses()
	if err != nil {
		return nil
	}
	ipSuggestions := make([]prompt.Suggest, len(ips)*2)
	j := 0
	for i := 0; i < len(ipSuggestions); i += 2 {
		ipSuggestions[i].Text = ips[j].Ip
		ipSuggestions[i].Description = ips[j].Name
		ipSuggestions[i+1].Text = ips[j].Name
		ipSuggestions[i+1].Description = ips[j].Ip
		j += 1
	}
	return ipSuggestions
}

func completer(in prompt.Document) []prompt.Suggest {
	line := in.CurrentLine()
	data := strings.Split(line, " ")
	if len(data) > 1 {
		lineSuggestions := make([]prompt.Suggest, 0)
		switch data[0] {
		case "send_data", "sd", "send_file", "sf":
			if len(data) == 2 {
				lineSuggestions = ipCompleter()
			}
		case "request_response", "rr":
			if len(data) == 2 {
				lineSuggestions = pfCompleter()
			}
		case "get_data", "gd", "get_file", "gf":
			if len(data) == 2 {
				lineSuggestions = rfCompleter()
			}
		case "received_file_info", "rfi":
			lineSuggestions = rfCompleter()
		case "processing_file_info", "pfi":
			lineSuggestions = pfCompleter()
		}
		return prompt.FilterHasPrefix(lineSuggestions, in.GetWordBeforeCursor(), true)
	}
	w := in.GetWordBeforeCursor()
	if w == "" {
		return []prompt.Suggest{}
	}
	return prompt.FilterHasPrefix(suggestions, w, true)
}

func checkExit(in string, breakline bool) bool {
	return exit
}

func mapToSuggestions() []prompt.Suggest {
	suggestions := make([]prompt.Suggest, len(CommandHelp))
	i := 0
	for k, v := range CommandHelp {
		suggestions[i].Text = k
		suggestions[i].Description = v
		i += 1
	}
	return suggestions
}
