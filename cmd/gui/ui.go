package main

import (
	"fmt"
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/inkyblackness/imgui-go"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/kohlten/fileSelector"
	"gitlab.com/kohlten/gamelib"
	"gitlab.com/kohlten/glLib"
	imguiImpl "gitlab.com/kohlten/phantomenvoy/cmd/gui/imgui-impl"
	"gitlab.com/kohlten/phantomenvoy/internal"
	"gitlab.com/kohlten/phantomenvoy/internal/events"
	"gitlab.com/kohlten/phantomenvoy/internal/file"
	"gitlab.com/kohlten/phantomenvoy/internal/globals"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

type uiFonts struct {
	header    imgui.Font
	subHeader imgui.Font
	text      imgui.Font
}

type UI struct {
	Window   *glLib.Window
	Platform *imguiImpl.SDL
	Renderer *imguiImpl.OpenGL3
	Ctx      *imgui.Context
	Io       imgui.IO
	fonts    uiFonts
	icon     imgui.TextureID
	backend  *internal.Phantom
	state    phantomState
	running  bool
}

func NewUI() (*UI, error) {
	ui := new(UI)
	backend, err := internal.NewPhantom()
	if err != nil {
		return nil, err
	}
	ui.backend = backend
	err = ui.state.init(ui.backend)
	if err != nil {
		return nil, err
	}
	err = ui.initDisplay()
	if err != nil {
		return nil, err
	}
	ui.initFonts()
	ui.Renderer.CreateFontsTexture()
	fileSelector.Init()
	err = ui.initImages()
	if err != nil {
		return nil, err
	}
	ui.running = true
	return ui, nil
}

func (ui *UI) Run() {
	for ui.running {
		ui.input()
		ui.update()
		ui.draw()
	}
}

func (ui *UI) Close() {
	ui.Window.Free()
}

func (ui *UI) initDisplay() error {
	settings := glLib.NewWindowSettings()
	settings.Name = "Phantom Envoy"
	settings.Width = 750
	settings.Height = 656
	settings.Monitor = 0
	settings.OpenGlVersionMajor = 3
	settings.OpenGlVersionMinor = 2
	settings.UseGl = true
	settings.Vsync = true
	window, err := glLib.NewWindow(settings)
	if err != nil {
		return err
	}
	ui.Window = window
	ui.Ctx = imgui.CreateContext(nil)
	ui.Io = imgui.CurrentIO()
	ui.Platform = imguiImpl.NewSDL(ui.Io, window.Window)
	ui.Renderer = imguiImpl.NewOpenGL3(ui.Io)
	return nil
}

func (ui *UI) initFonts() {
	fonts := ui.Io.Fonts()
	ui.fonts.text = fonts.AddFontFromFileTTFV(globals.BaseDir + "/assets/OpenSans-Regular.ttf", 17, imgui.DefaultFontConfig, fonts.GlyphRangesDefault())
	ui.fonts.subHeader = fonts.AddFontFromFileTTFV(globals.BaseDir + "/assets/OpenSans-Regular.ttf", 22, imgui.DefaultFontConfig, fonts.GlyphRangesDefault())
	ui.fonts.header = fonts.AddFontFromFileTTFV(globals.BaseDir + "/assets/OpenSans-Regular.ttf", 30, imgui.DefaultFontConfig, fonts.GlyphRangesDefault())
}

func (ui *UI) initImages() error {
	surf, err := gamelib.NewSurfaceFromFile(globals.BaseDir + "/assets/PE_ghosty_wip.png")
	if err != nil {
		return err
	}
	ui.Window.Window.SetIcon(surf.GetSDLSurf())
	size := surf.Size
	tex := glLib.NewTextureFromMemory(surf.GetRawPixels(), int32(size.X), int32(size.Y), gl.RGBA, gl.REPEAT, gl.REPEAT, gl.LINEAR,
		gl.LINEAR)
	ui.icon = imgui.TextureID(tex.GetID())
	ui.Renderer.AddImage(tex.GetID())
	assets := fileSelector.DefaultDarkAssets
	ui.Renderer.AddImage(uint32(assets.Folder))
	ui.Renderer.AddImage(uint32(assets.Arrow))
	ui.Renderer.AddImage(uint32(assets.Refresh))
	ui.Renderer.AddImage(uint32(assets.Hidden))
	assets = fileSelector.DefaultLightAssets
	ui.Renderer.AddImage(uint32(assets.Folder))
	ui.Renderer.AddImage(uint32(assets.Arrow))
	ui.Renderer.AddImage(uint32(assets.Refresh))
	ui.Renderer.AddImage(uint32(assets.Hidden))
	return nil
}

func (ui *UI) input() {
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		ui.Platform.ProcessEvent(event)
		switch event.(type) {
		case *sdl.QuitEvent:
			ui.running = false
		}
		for _, sendE := range ui.state.sendWindows {
			sendE.selector.Input(event)
		}
		for i := 0; i < len(ui.state.results); i++ {
			ui.state.results[i].selector.Input(event)
		}
	}
}

func (ui *UI) update() {
	ui.state.update(ui.backend)
	ui.state.updateModules(ui.backend)
}

func (ui *UI) draw() {
	ui.Platform.NewFrame()
	imgui.NewFrame()
	ui.Renderer.PreRender([3]float32{})
	ui.drawUI()
	imgui.Render()
	ui.Renderer.Render(ui.Platform.DisplaySize(), ui.Platform.FramebufferSize(), imgui.RenderedDrawData())
	ui.Window.Window.GLSwap()
}

func (ui *UI) drawUI() {
	topBarSize := imgui.Vec2{X: 753, Y: 69}
	imgui.SetNextWindowPosV(imgui.Vec2{}, imgui.ConditionFirstUseEver, imgui.Vec2{})
	imgui.SetNextWindowSize(topBarSize)
	imgui.BeginV("topBar", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize|imgui.WindowFlagsNoBringToFrontOnFocus)
	imgui.PushFont(ui.fonts.header)
	imgui.Dummy(imgui.Vec2{X: 10})
	imgui.SameLine()
	imgui.Image(ui.icon, imgui.Vec2{X: 50, Y: 50})
	imgui.SameLine()
	imgui.SameLineV(0, 16)
	imgui.Text("PhantomEnvoy")
	imgui.PopFont()
	imgui.End()
	middleBarSize := imgui.Vec2{X: 612, Y: 200}
	imgui.SetNextWindowPosV(imgui.Vec2{Y: topBarSize.Y + 5}, imgui.ConditionAlways, imgui.Vec2{})
	imgui.SetNextWindowSize(middleBarSize)
	imgui.BeginV("middleBar", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize|imgui.WindowFlagsNoBringToFrontOnFocus)
	imgui.PushFont(ui.fonts.subHeader)
	imgui.Text("Available")
	imgui.PopFont()
	imgui.SameLine()
	imgui.Dummy(imgui.Vec2{X: middleBarSize.X - (imgui.CalcTextSize("Refresh", false,
		0).X + 63 + imgui.CalcTextSize("Available", false, 0).X)})
	imgui.SameLine()
	imgui.PushFont(ui.fonts.subHeader)
	if imgui.Button("Refresh") {
		_ = ui.backend.ClearAddresses()
	}
	imgui.PopFont()
	imgui.Separator()
	ui.drawAvailable()
	imgui.End()
	modulesBarSize := imgui.Vec2{X: 135, Y: 200}
	imgui.SetNextWindowPosV(imgui.Vec2{X: middleBarSize.X + 5, Y: topBarSize.Y + 5}, imgui.ConditionAlways, imgui.Vec2{})
	imgui.SetNextWindowSize(modulesBarSize)
	imgui.BeginV("modulesBar", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize|imgui.WindowFlagsNoBringToFrontOnFocus)
	imgui.PushFont(ui.fonts.subHeader)
	imgui.Dummy(imgui.Vec2{X: (modulesBarSize.X/2 - imgui.CalcTextSize("Modules", false, 0).X/2) - 8})
	imgui.SameLine()
	imgui.Text("Modules")
	imgui.PopFont()
	imgui.Separator()
	imgui.Dummy(imgui.Vec2{Y: 15})
	imgui.Dummy(imgui.Vec2{X: 15})
	imgui.SameLine()
	imgui.PushFont(ui.fonts.text)
	imgui.Checkbox("Listener", &ui.state.listenerActive)
	imgui.Dummy(imgui.Vec2{Y: 25})
	imgui.Dummy(imgui.Vec2{X: 15})
	imgui.SameLine()
	imgui.Checkbox("Sender", &ui.state.senderActive)
	imgui.Dummy(imgui.Vec2{Y: 25})
	imgui.Dummy(imgui.Vec2{X: 15})
	imgui.SameLine()
	imgui.Checkbox("Receiver", &ui.state.receiverActive)
	imgui.PopFont()
	imgui.End()
	bottomBarSize := imgui.Vec2{X: 753, Y: 186}
	imgui.SetNextWindowPosV(imgui.Vec2{Y: middleBarSize.Y + topBarSize.Y + 10}, imgui.ConditionAlways, imgui.Vec2{})
	imgui.SetNextWindowSize(bottomBarSize)
	imgui.BeginV("sendingBar", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize|imgui.WindowFlagsNoBringToFrontOnFocus)
	imgui.PushFont(ui.fonts.subHeader)
	imgui.Text("Sending")
	imgui.PopFont()
	imgui.Separator()
	ui.drawProcessing(ui.state.sending, bottomBarSize, "sending")
	imgui.End()
	imgui.SetNextWindowPosV(imgui.Vec2{Y: middleBarSize.Y + topBarSize.Y + bottomBarSize.Y + 15}, imgui.ConditionAlways, imgui.Vec2{})
	imgui.SetNextWindowSize(bottomBarSize)
	imgui.BeginV("receivingBar", nil, imgui.WindowFlagsNoTitleBar|imgui.WindowFlagsNoMove|imgui.WindowFlagsNoResize|imgui.WindowFlagsNoBringToFrontOnFocus)
	imgui.PushFont(ui.fonts.subHeader)
	imgui.Text("Receiving")
	imgui.PopFont()
	imgui.Separator()
	ui.drawProcessing(ui.state.receiving, bottomBarSize, "receiving")
	imgui.End()
	ui.drawWindows()
}

func (ui *UI) drawAvailable() {
	imgui.BeginChild("##Available")
	imgui.PushFont(ui.fonts.text)
	for i := 0; i < len(ui.state.available); i++ {
		c := ui.state.available[i]
		imgui.Text(c.Ip)
		imgui.SameLine()
		imgui.Dummy(imgui.Vec2{X: imgui.CalcTextSize("000.000.000.000", false, 0).X - imgui.CalcTextSize(c.Ip, false, 0).X})
		imgui.SameLine()
		imgui.Text(c.Name)
		imgui.SameLine()
		imgui.Dummy(imgui.Vec2{X: imgui.CalcTextSize(strings.Repeat("0", 60), false, 0).X - imgui.CalcTextSize(c.Name, false, 0).X})
		imgui.SameLine()
		if imgui.Button("Send##" + c.Ip) {
			found := false
			for i := 0; i < len(ui.state.sendWindows); i++ {
				if ui.state.sendWindows[i].name == c.Ip {
					found = true
				}
			}
			if !found {
				selector := fileSelector.NewFileSelector(c.Ip, "", fileSelector.ModeOpenFiles, imgui.Vec2{X: 600, Y: 400},
					nil, imgui.WindowFlagsNoTitleBar, fileSelector.ThemeLight)
				selector.SetOpen(false)
				ui.state.sendWindows = append(ui.state.sendWindows, &sendEvent{name: c.Ip, selector: selector})
			}
		}
	}
	imgui.PopFont()
	imgui.EndChild()
}

func (ui *UI) drawProcessing(fileInfo []*file.Info, windowSize imgui.Vec2, id string) {
	imgui.BeginChild("##Processing_" + id)
	imgui.PushFont(ui.fonts.text)
	for i := 0; i < len(fileInfo); i++ {
		info := fileInfo[i]
		imgui.Text(info.Id)
		imgui.SameLine()
		imgui.Text(info.Name)
		imgui.SameLine()
		size := (windowSize.X - (imgui.CalcTextSize(info.Name, false, 0).X + imgui.CalcTextSize(info.Id+"  ", false, 0).X)) - 220
		imgui.Dummy(imgui.Vec2{X: size})
		imgui.SameLine()
		imgui.ProgressBarV(float32(info.Chunks)/float32(info.TotalChunks), imgui.Vec2{X: 150, Y: 17}, "")
		imgui.SameLine()
		imgui.Dummy(imgui.Vec2{X: 10})
		imgui.SameLine()
		if imgui.Button("X") {
			ui.backend.Cancel(info.Id)
		}
	}
	imgui.PopFont()
	imgui.EndChild()
}

func (ui *UI) drawWindows() {
	for i := 0; i < len(ui.state.requests); i++ {
		if ui.drawRequestWindow(ui.state.requests[i]) {
			ui.state.requests = append(ui.state.requests[:i], ui.state.requests[i+1:]...)
		} else {
			i++
		}
	}
	for i := 0; i < len(ui.state.results); i++ {
		if ui.drawResultWindow(ui.state.results[i]) {
			ui.state.results[i].selector.Finish()
			ui.state.results = append(ui.state.results[:i], ui.state.results[i+1:]...)
		} else {
			i++
		}
	}
	for i := 0; i < len(ui.state.errors); {
		if ui.drawErrorWindow(ui.state.errors[i]) {
			ui.state.errors = append(ui.state.errors[:i], ui.state.errors[i+1:]...)
		} else {
			i++
		}
	}
	for i := 0; i < len(ui.state.sendWindows); {
		if ui.drawSendWindow(ui.state.sendWindows[i]) {
			ui.state.sendWindows[i].selector.Finish()
			ui.state.sendWindows = append(ui.state.sendWindows[:i], ui.state.sendWindows[i+1:]...)
		} else {
			i++
		}
	}
}

func (ui *UI) drawRequestWindow(request *events.RequestToSend) bool {
	ret := false
	size := imgui.Vec2{X: 500, Y: 155}
	width, height := ui.Window.GetSize()
	imgui.SetNextWindowPosV(imgui.Vec2{X: float32(width/2) - size.X/2, Y: float32(height/2) - size.Y/2}, imgui.ConditionAppearing, imgui.Vec2{})
	imgui.SetNextWindowSize(size)
	imgui.BeginV("Incoming Request##"+request.Id, nil, imgui.WindowFlagsNoResize|imgui.WindowFlagsNoCollapse)
	imgui.PushTextWrapPosV(size.X)
	imgui.Text("Name: " + request.Filename)
	imgui.Text("ID: " + request.Id)
	imgui.Text("Hash: " + request.Hash)
	imgui.Text("Size: " + strconv.FormatUint(uint64(request.FileSize), 10))
	imgui.PopTextWrapPos()
	imgui.Dummy(imgui.Vec2{Y: 5})
	imgui.Dummy(imgui.Vec2{X: size.X/4 - (imgui.CalcTextSize("Deny", false, 0).X + 20)})
	imgui.SameLine()
	if imgui.Button("Deny") {
		ui.backend.Push(&events.RequestResult{Id: request.Id, Result: false})
		ret = true
	}
	imgui.SameLine()
	imgui.Dummy(imgui.Vec2{X: ((size.X / 4) * 2) - (imgui.CalcTextSize("Accept", false, 0).X + 20)})
	imgui.SameLine()
	if imgui.Button("Accept") {
		ui.backend.Push(&events.RequestResult{Id: request.Id, Result: true})
		ret = true
	}
	imgui.End()
	return ret
}

func (ui *UI) drawResultWindow(result *resultEvent) bool {
	ret := false
	size := imgui.Vec2{X: 500, Y: 400}
	width, height := ui.Window.GetSize()
	imgui.SetNextWindowPosV(imgui.Vec2{X: float32(width/2) - size.X/2, Y: float32(height/2) - size.Y/2}, imgui.ConditionAppearing, imgui.Vec2{})
	imgui.SetNextWindowSize(size)
	imgui.BeginV("Result##"+result.id, nil, imgui.WindowFlagsNoResize|imgui.WindowFlagsNoCollapse)
	if !result.result {
		text := result.completed.Name + " with an id of " + result.id + " finished unsuccessfully."
		imgui.PushTextWrapPosV(size.X - 40)
		imgui.Dummy(imgui.Vec2{X: 20})
		imgui.SameLine()
		imgui.Text(text)
		textSize := imgui.CalcTextSize(text, false, size.X-40)
		buttonSize := imgui.CalcTextSize("Ok", false, 0)
		imgui.Dummy(imgui.Vec2{Y: size.Y - (textSize.Y + buttonSize.X + 20)})
		imgui.Dummy(imgui.Vec2{X: size.X - (buttonSize.X + 20)})
		imgui.SameLine()
		if imgui.Button("Ok##" + result.id) {
			ret = true
		}
	} else {
		if imgui.BeginTabBar("##fileSelector") {
			if imgui.BeginTabItem("File") {
				imgui.Dummy(imgui.Vec2{Y: 125})
				imgui.Dummy(imgui.Vec2{X: 5})
				imgui.SameLine()
				imgui.Text("Filename: ")
				imgui.SameLine()
				imgui.InputText("##"+result.id, &result.outputDir)
				imgui.SameLine()
				imgui.Dummy(imgui.Vec2{X: 5})
				imgui.SameLine()
				if imgui.Button("Open##" + result.id) {
					result.selector.SetOpen(true)
				}
				if !result.selector.IsOpen() && result.selector.HasSelected() {
					result.outputDir = result.selector.GetSelected()
				}
				imgui.Dummy(imgui.Vec2{Y: 145})
				imgui.Dummy(imgui.Vec2{X: 50})
				imgui.SameLine()
				if imgui.Button("Quit##" + result.id + "File") {
					_ = os.Remove(result.completed.OutputName)
					ret = true
				}
				imgui.SameLine()
				imgui.Dummy(imgui.Vec2{X: size.X - 225})
				imgui.SameLine()
				if imgui.Button("Ok##" + result.id + "File") {
					if result.outputDir != "" {
						if result.completed != nil {
							err := os.Rename(result.completed.OutputName, result.outputDir + "/" + result.completed.Name)
							if err != nil {
								log.Println("Failed to move ", result.completed.Name, " to ", result.outputDir, " with error ", err.Error())
							}
						}
						ret = true
					}
				}
				imgui.EndTabItem()
			}
			if imgui.BeginTabItem("Text") {
				if result.canPreview {
					imgui.InputTextMultilineV("##"+result.id, &result.data, imgui.Vec2{X: 500, Y: 303}, imgui.InputTextFlagsReadOnly, nil)
					imgui.Dummy(imgui.Vec2{X: 50})
					imgui.SameLine()
					if imgui.Button("Quit##" + result.id + "Text") {
						ret = true
					}
				} else {
					errorString := "Faild to preview file " + result.completed.Name
					textSize := imgui.CalcTextSize(errorString, false, size.X - 20)
					imgui.PushTextWrapPosV(size.X - 20)
					imgui.Dummy(imgui.Vec2{Y: (size.Y / 2) - textSize.Y})
					imgui.Dummy(imgui.Vec2{X: size.X / 2 - textSize.X / 2})
					imgui.SameLine()
					imgui.Text(errorString)
					okSize := imgui.CalcTextSize("Ok", false, 0)
					imgui.Dummy(imgui.Vec2{Y: size.Y / 2 - (okSize.Y + 30 + textSize.Y)})
					imgui.Dummy(imgui.Vec2{X: size.X / 2 - okSize.X / 2})
					imgui.SameLine()
					if imgui.Button("Ok##" + result.id) {
						ret = true
					}
				}
				imgui.EndTabItem()
			}
			imgui.EndTabBar()
		}
	}
	imgui.End()
	result.selector.Draw()
	return ret
}

func (ui *UI) drawErrorWindow(error *events.Error) bool {
	ret := false
	size := imgui.Vec2{X: 300, Y: 75}
	width, height := ui.Window.GetSize()
	imgui.SetNextWindowPosV(imgui.Vec2{X: float32(width/2) - size.X/2, Y: float32(height/2) - size.Y/2}, imgui.ConditionAppearing, imgui.Vec2{})
	imgui.SetNextWindowSize(size)
	imgui.BeginV("Error##"+error.Reason, nil, imgui.WindowFlagsNoResize|imgui.WindowFlagsNoCollapse)
	imgui.PushFont(ui.fonts.subHeader)
	imgui.Text("Error: ")
	imgui.PopFont()
	imgui.PushFont(ui.fonts.text)
	value := error.Reason
	if error.Ip != "" {
		value = error.Ip + ": " + value
	}
	imgui.PushTextWrapPosV(250)
	imgui.Text(value)
	imgui.PopTextWrapPos()
	if imgui.Button("Ok##" + error.Reason) {
		ret = true
	}
	imgui.PopFont()
	imgui.End()
	return ret
}

func (ui *UI) drawSendWindow(send *sendEvent) bool {
	ret := false
	size := imgui.Vec2{X: 500, Y: 400}
	width, height := ui.Window.GetSize()
	imgui.SetNextWindowPosV(imgui.Vec2{X: float32(width/2) - size.X/2, Y: float32(height/2) - size.Y/2}, imgui.ConditionAppearing, imgui.Vec2{})
	imgui.SetNextWindowSizeV(size, imgui.ConditionAppearing)
	imgui.BeginV("Send##"+send.name, nil, imgui.WindowFlagsNoCollapse|imgui.WindowFlagsNoResize)
	if imgui.BeginTabBar("##fileSelector") {
		if imgui.BeginTabItem("File") {
			imgui.Dummy(imgui.Vec2{Y: 125})
			imgui.Dummy(imgui.Vec2{X: 5})
			imgui.SameLine()
			imgui.Text("Filename: ")
			imgui.SameLine()
			imgui.InputText("##"+send.name, &send.filename)
			imgui.SameLine()
			imgui.Dummy(imgui.Vec2{X: 5})
			imgui.SameLine()
			if imgui.Button("Open##" + send.name) {
				send.selector.SetOpen(true)
			}
			if !send.selector.IsOpen() && send.selector.HasSelected() {
				send.filename = send.selector.GetSelected()
			}
			imgui.Dummy(imgui.Vec2{Y: 145})
			imgui.Dummy(imgui.Vec2{X: 50})
			imgui.SameLine()
			if imgui.Button("Quit##" + send.name + "File") {
				ret = true
			}
			imgui.SameLine()
			imgui.Dummy(imgui.Vec2{X: size.X - 225})
			imgui.SameLine()
			if imgui.Button("Send##" + send.name + "File") {
				if send.filename != "" {
					go ui.sendFile(send)
					ret = true
				}
			}
			imgui.EndTabItem()
		}
		if imgui.BeginTabItem("Text") {
			imgui.InputTextMultilineV("##"+send.name, &send.data, imgui.Vec2{X: 500, Y: 303}, 0, nil)
			imgui.Dummy(imgui.Vec2{X: 50})
			imgui.SameLine()
			if imgui.Button("Quit##" + send.name + "Text") {
				ret = true
			}
			imgui.SameLine()
			imgui.Dummy(imgui.Vec2{X: size.X - 225})
			imgui.SameLine()
			if imgui.Button("Send##" + send.name + "Text") {
				go ui.sendData(send)
				ret = true
			}
			imgui.EndTabItem()
		}
		imgui.EndTabBar()
	}
	imgui.End()
	send.selector.Draw()
	return ret
}

func (ui *UI) sendData(send *sendEvent) {
	tmp, err := ioutil.TempFile("", "")
	if err != nil {
		ui.state.errors = append(ui.state.errors, &events.Error{Reason: "Failed to send with error " + err.Error()})
		return
	}
	_, err = tmp.Write([]byte(send.data))
	if err != nil {
		ui.state.errors = append(ui.state.errors, &events.Error{Reason: "Failed to send with error " + err.Error()})
		_ = tmp.Close()
		_ = os.Remove(tmp.Name())
		return
	}
	tmpName := tmp.Name()
	err = ui.backend.Send(send.name, tmpName)
	if err != nil {
		ui.state.errors = append(ui.state.errors, &events.Error{Reason: "Failed to send with error " + err.Error()})
		_ = tmp.Close()
		_ = os.Remove(tmp.Name())
		return
	}
}

func (ui *UI) sendFile(send *sendEvent) {
	files := strings.Split(send.filename, ",")
	for _, file := range files {
		if _, err := os.Stat(file); os.IsNotExist(err) {
			ui.state.errors = append(ui.state.errors, &events.Error{Reason: fmt.Sprintf("File %s does not exists",
				file)})
			continue
		}
		err := ui.backend.Send(send.name, file)
		if err != nil {
			ui.state.errors = append(ui.state.errors, &events.Error{Reason: "Failed to send with error " + err.Error()})
		}
	}
}
