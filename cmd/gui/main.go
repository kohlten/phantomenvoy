package main

import (
	"log"
	"runtime"
)

func init() {
	runtime.LockOSThread()
}

func main() {
	ui, err := NewUI()
	if err != nil {
		log.Fatal("Error creating ui: ", err)
	}
	ui.Run()
	ui.Close()
}