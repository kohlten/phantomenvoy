package main

import (
	"gitlab.com/kohlten/phantomenvoy/internal/file"
)

type fileList []*file.Info

func (list fileList) Len() int {
	return len(list)
}

func (list fileList) Less(i, j int) bool {
	return list[i].Id < list[j].Id
}

func (list fileList) Swap(i, j int) {
	list[i], list[j] = list[j], list[i]
}

