package main

import (
	"fmt"
	"github.com/inkyblackness/imgui-go"
	"gitlab.com/kohlten/fileSelector"
	"gitlab.com/kohlten/phantomenvoy/internal"
	"gitlab.com/kohlten/phantomenvoy/internal/clientBank"
	"gitlab.com/kohlten/phantomenvoy/internal/events"
	"gitlab.com/kohlten/phantomenvoy/internal/file"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"time"
)

const (
	searchIter = int64(700)

	maxPreviewSize = 1000
)

type sendEvent struct {
	name         string
	filename     string
	data         string
	showSelector bool
	selector     *fileSelector.FileSelector
}

type resultEvent struct {
	completed  *file.CompletedFile
	outputDir  string
	data       string
	id         string
	selector   *fileSelector.FileSelector
	canPreview bool
	result     bool
}

type phantomState struct {
	previousListener bool
	previousSender   bool
	previousReceiver bool
	listenerActive   bool
	senderActive     bool
	receiverActive   bool
	available        []*clientBank.Client
	sending          []*file.Info
	receiving        []*file.Info
	results          []*resultEvent
	sendWindows      []*sendEvent
	requests         []*events.RequestToSend
	errors           []*events.Error
	lastSearch       int64
}

func (state *phantomState) init(phantom *internal.Phantom) error {
	state.previousListener = true
	state.previousSender = true
	state.previousReceiver = true
	state.listenerActive = true
	state.senderActive = true
	state.receiverActive = true
	err := phantom.StartListener()
	if err != nil {
		return err
	}
	err = phantom.StartSender()
	if err != nil {
		return err
	}
	err = phantom.StartReceiver()
	if err != nil {
		return err
	}
	state.lastSearch = time.Now().UnixNano() / int64(time.Millisecond)
	return nil
}

func (state *phantomState) update(phantom *internal.Phantom) {
	for event := phantom.Poll(); event != nil; event = phantom.Poll() {
		switch e := event.(type) {
		case *events.SendResult:
			result := wrapResultEvent(e, phantom)
			if result.completed == nil {
				continue
			}
			found := false
			for i := 0; i < len(state.results); i++ {
				if state.results[i].completed.Name == result.completed.Name {
					found = true
				}
			}
			if !found {
				state.results = append(state.results, result)
			} else {
				result.selector.Finish()
			}
		case *events.RequestToSend:
			state.requests = append(state.requests, e)
		case *events.Error:
			state.errors = append(state.errors, e)
		default:
			fmt.Println(e)
		}
	}
	current := time.Now().UnixNano() / int64(time.Millisecond)
	if current-state.lastSearch >= searchIter && state.listenerActive {
		_ = phantom.Search()
		state.lastSearch = current
	}
	available, err := phantom.GetAddresses()
	if err == nil {
		state.available = available
	}
	state.receiving = phantom.GetProcessingFileInfo()
	sort.Sort(fileList(state.receiving))
	state.sending = phantom.GetSendingFileInfo()
	sort.Sort(fileList(state.sending))
}

func (state *phantomState) updateModules(phantom *internal.Phantom) {
	for i := 0; i < len(state.sendWindows); i++ {
		state.sendWindows[i].selector.Update()
	}
	for i := 0; i < len(state.results); i++ {
		state.results[i].selector.Update()
	}
	if state.previousListener != state.listenerActive {
		if state.listenerActive {
			err := phantom.StartListener()
			if err != nil {
				log.Println("Error switching: ", err)
			} else {
				_ = phantom.ClearAddresses()
				state.available = nil
			}
		} else {
			_ = phantom.ClearAddresses()
			state.available = nil
			err := phantom.StopListener()
			if err != nil {
				log.Println("Error switching: ", err)
			}
		}
		state.previousListener = state.listenerActive
	}
	if state.previousSender != state.senderActive {
		if state.senderActive {
			err := phantom.StartSender()
			if err != nil {
				log.Println("Error switching: ", err)
			}
		} else {
			err := phantom.StopSender()
			if err != nil {
				log.Println("Error switching: ", err)
			}
		}
		state.previousSender = state.senderActive
	}
	if state.previousReceiver != state.receiverActive {
		if state.receiverActive {
			err := phantom.StartReceiver()
			if err != nil {
				log.Println("Error switching: ", err)
			}
		} else {
			err := phantom.StopReceiver()
			if err != nil {
				log.Println("Error switching: ", err)
			}
		}
		state.previousReceiver = state.receiverActive
	}
}

func wrapResultEvent(e *events.SendResult, backend *internal.Phantom) *resultEvent {
	result := new(resultEvent)
	result.result = e.Result
	result.id = e.Id
	previewData(result, backend, e.Id)
	result.selector = fileSelector.NewFileSelector(e.Id, "", fileSelector.ModeOpenDirectory, imgui.Vec2{X: 600, Y: 400},
		nil, imgui.WindowFlagsNoTitleBar, fileSelector.ThemeLight)
	result.selector.SetOpen(false)
	return result
}

func previewData(event *resultEvent, backend *internal.Phantom, id string) {
	completed, err := backend.GetFile(id)
	if err != nil {
		event.result = false
		return
	}
	event.completed = completed
	fd, err := os.Open(completed.OutputName)
	if err != nil {
		return
	}
	stat, err := fd.Stat()
	if err != nil {
		_ = fd.Close()
		return
	}
	if stat.Size() > maxPreviewSize {
		return
	}
	data, err := ioutil.ReadAll(fd)
	if err != nil {
		_ = fd.Close()
		return
	}
	_ = fd.Close()
	if !canPreview(data) {
		return
	}
	event.data = string(data)
	event.canPreview = true
}

func canPreview(data []byte) bool {
	for _, c := range data {
		if c < 32 || c > 126 {
			return false
		}
	}
	return true
}
