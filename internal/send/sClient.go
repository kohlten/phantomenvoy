package send

import (
	"bytes"
	"crypto/sha256"
	"crypto/tls"
	"encoding/hex"
	"fmt"
	"github.com/google/uuid"
	phantomEvents "gitlab.com/kohlten/phantomenvoy/internal/events"
	"gitlab.com/kohlten/phantomenvoy/internal/file"
	"gitlab.com/kohlten/phantomenvoy/internal/fileIndexer"
	"gitlab.com/kohlten/phantomenvoy/internal/globals"
	"gitlab.com/kohlten/phantomenvoy/internal/packet"
	"gitlab.com/kohlten/phantomenvoy/internal/packet/format"
	"gitlab.com/kohlten/protocol/client"
	protocolEvents "gitlab.com/kohlten/protocol/events"
	"gitlab.com/kohlten/protocol/util"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"time"
)

const (
	DataHeaderSize = 103
	PacketsPer     = 32
	maxWaitTime    = 300000
)

type sClient struct {
	queue      []*file.File
	queueMutex *sync.Mutex
	sock       *client.Client
	buffer     []byte
	running    bool
	stopped    bool
	lastUpdate uint
	hasHadFile bool
}

func newSClient(ip string) (*sClient, error) {
	c := new(sClient)
	config := tls.Config{InsecureSkipVerify: true}
	sock, err := tls.Dial("tcp", ip+":"+strconv.FormatInt(globals.ServerPort, 10), &config)
	if err != nil {
		return nil, err
	}
	c.sock = client.NewClient(sock, nil)
	c.running = true
	c.queueMutex = &sync.Mutex{}
	c.lastUpdate = uint(util.Timestamp())
	go c.update()
	return c, nil
}

func (c *sClient) hashFile(filename string) (string, error) {
	fd, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	hash, err := c.hashData(fd)
	if err != nil {
		return "", err
	}
	_ = fd.Close()
	return hash, nil
}

func (c *sClient) hashData(buffer io.Reader) (string, error) {
	hasher := sha256.New()
	_, err := io.Copy(hasher, buffer)
	if err != nil {
		return "", err
	}
	h := hasher.Sum(nil)
	hash := hex.EncodeToString(h)
	return hash, nil
}

func (c *sClient) send(filename string) error {
	id := uuid.New().String()
	indexer, err := fileIndexer.NewFileIndexer(uint(format.MaxPacketSize-DataHeaderSize)-100, filename)
	if err != nil {
		return err
	}
	hash, err := c.hashFile(filename)
	if err != nil {
		return err
	}
	f := file.File{
		Size:      indexer.GetLength(),
		Name:      filepath.Base(filename),
		Hash:      hash,
		ChunksLen: indexer.GetChunkLength(),
		Id:        id,
		Indexer:   indexer,
		Allowed:   false,
	}
	c.queueMutex.Lock()
	c.queue = append(c.queue, &f)
	c.queueMutex.Unlock()
	packet.SendRequestToSend(c.sock, 0, &f)
	c.hasHadFile = true
	return nil
}

func (c *sClient) getSendingFileInfo() []*file.Info {
	c.queueMutex.Lock()
	defer c.queueMutex.Unlock()
	infoArr := make([]*file.Info, len(c.queue))
	for i := 0; i < len(c.queue); i++ {
		f := c.queue[i]
		info := new(file.Info)
		info.Name = f.Name
		info.Size = f.Size
		info.Id = f.Id
		info.Chunks = f.Sent
		info.TotalChunks = f.ChunksLen
		info.Hash = f.Hash
		infoArr[i] = info
	}
	return infoArr
}

func (c *sClient) update() {
	for c.running {
		if len(c.queue) == 0 && c.hasHadFile {
			c.running = false
		}
		if uint(util.Timestamp())-c.lastUpdate > maxWaitTime {
			c.sendError("took too long")
		}
		if c.sock.GetErr() != nil {
			c.sendError(c.sock.GetErr().Error())
		}
		for event := c.sock.Events.Poll(); event != nil; event = c.sock.Events.Poll() {
			packetEvent := event.(*protocolEvents.ReceivedPacketEvent)
			phantomEvent, err := packet.ParsePacket(packetEvent.Packet, packetEvent.Id)
			if err != nil {
				c.sendError(fmt.Sprint("failed to parse packet from sClient ", packetEvent.Id))
			}
			c.lastUpdate = uint(util.Timestamp())
			c.queueMutex.Lock()
			switch e := phantomEvent.(type) {
			case *phantomEvents.SendResult:
				for i := 0; i < len(c.queue); i++ {
					if e.Id == c.queue[i].Id {
						e.Filename = c.queue[i].Name
						packet.SendFailed(c.sock, 0, c.queue[i].Id)
						c.queue = append(c.queue[:i], c.queue[i+1:]...)
						break
					}
				}
				globals.Events.Push(e)
			case *phantomEvents.RequestResult:
				for i := 0; i < len(c.queue); i++ {
					if e.Id == c.queue[i].Id {
						if e.Result {
							c.queue[i].Allowed = true
							c.sendChunk(c.queue[i], 0)
							c.queue[i].Sent += 1
							break
						} else {
							packet.SendFailed(c.sock, 0, c.queue[i].Id)
							c.queue = append(c.queue[:i], c.queue[i+1:]...)
							break
						}
					}
				}
			case *phantomEvents.Resend:
				for i := 0; i < len(c.queue); i++ {
					if e.Id == c.queue[i].Id {
						if e.Index > c.queue[i].ChunksLen {
							globals.Events.Push(&phantomEvents.Error{
								Ip:     c.sock.GetRemoteAddr(),
								Reason: "resend index is greater than that total chunks",
							})
							break
						}
						if e.Result {
							c.sendData(c.queue[i], e.Index)
						} else if e.Index < c.queue[i].ChunksLen-1 {
							c.sendChunk(c.queue[i], e.Index)
						}
						break
					}
				}
			}
			c.queueMutex.Unlock()
		}
	}
	c.stopped = true
}

func (c *sClient) sendChunks(f *file.File, index uint, zero bool) {
	size := uint(PacketsPer)
	if f.ChunksLen-index < PacketsPer {
		size = f.ChunksLen - index
	}
	j := index
	if !zero {
		j++
	}
	for ; j < index+size; j++ {
		c.sendData(f, j)
	}
}

func (c *sClient) cancel(id string) {
	for i := 0; i < len(c.queue); i++ {
		if c.queue[i].Id == id {
			c.queue[i].Indexer.Close()
			packet.SendFailed(c.sock, 0, id)
			c.queueMutex.Lock()
			c.queue = append(c.queue[:i], c.queue[i+1:]...)
			c.queueMutex.Unlock()
		}
	}
}

func (c *sClient) sendError(reason string) {
	err := phantomEvents.Error{
		Ip:     c.sock.GetRemoteAddr(),
		Reason: reason,
	}
	globals.Events.Push(err)
	c.running = false
}

func (c *sClient) sendChunk(f *file.File, index uint) {
	size := uint(PacketsPer)
	if f.ChunksLen-index < PacketsPer {
		size = f.ChunksLen - index
	}
	j := index + 1
	if index == 0 {
		j = index
	}
	for ; j < index+size; j++ {
		c.sendData(f, j)
	}
	f.Sent += size
}

func (c *sClient) sendData(f *file.File, index uint) {
	data, err := f.Indexer.Get(index)
	if err != nil {
		fmt.Println("sClient Error: ", err)
		return
	}
	hash, err := c.hashData(bytes.NewBuffer(data))
	if err != nil {
		fmt.Println("sClient Error: ", err)
		return
	}
	packet.SendData(c.sock, 0, f.Id, hash, index, data)
}

func (c *sClient) close() {
	for len(c.queue) > 0 {
		for i := 0; i < len(c.queue); {
			c.cancel(c.queue[i].Id)
		}
		time.Sleep(50 * time.Millisecond)
	}
	c.running = false
	for !c.stopped {
		time.Sleep(5 * time.Millisecond)
	}
	c.sock.Close()
}
