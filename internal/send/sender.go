package send

import (
	"gitlab.com/kohlten/phantomenvoy/internal/file"
	"gitlab.com/kohlten/protocol/protocol"
	"sync"
	"time"
)

type Sender struct {
	clients map[string]*sClient
	clientMutex sync.Mutex
	running bool
}

func NewSender() *Sender {
	s := new(Sender)
	s.clients = make(map[string]*sClient)
	s.running = true
	protocol.SetMTU(2048)
	go s.update()
	return s
}

func (s *Sender) Send(ip, filename string) error {
	var err error

	c, ok := s.clients[ip]
	if !ok {
		c, err = newSClient(ip)
		if err != nil {
			return err
		}
		s.clients[ip] = c
	}
	return c.send(filename)
}

func (s *Sender) GetSendingFileInfo() []*file.Info {
	retInfo := make([]*file.Info, 0)
	s.clientMutex.Lock()
	defer s.clientMutex.Unlock()
	for _, c := range s.clients {
		info := c.getSendingFileInfo()
		retInfo = append(retInfo, info...)
	}
	return retInfo
}

func (s *Sender) Cancel(id string) {
	s.clientMutex.Lock()
	defer s.clientMutex.Unlock()
	for _, v := range s.clients {
		v.cancel(id)
	}
}

func (s *Sender) Close() {
	s.clientMutex.Lock()
	defer s.clientMutex.Unlock()
	for _, v := range s.clients {
		v.close()
	}
	s.running = false
	s.clients = nil
}

func (s *Sender) update() {
	for s.running {
		s.clientMutex.Lock()
		for key, client := range s.clients {
			if !client.running {
				client.close()
				delete(s.clients, key)
			}
		}
		s.clientMutex.Unlock()
		time.Sleep(1 * time.Millisecond)
	}
}