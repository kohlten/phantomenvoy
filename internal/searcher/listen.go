package searcher

import (
	"gitlab.com/kohlten/phantomenvoy/internal/packet/format"
	"net"
	"os"
	"strconv"
	"time"

	"gitlab.com/kohlten/phantomenvoy/internal/globals"
)

// @TODO Look into using UDP broadcast instead

type Listener struct {
	sock         *net.UDPConn
	running      bool
	finished     bool
	err          error
}

func NewListener() (*Listener, error) {
	l := new(Listener)
	addr, err := net.ResolveUDPAddr("udp", "0.0.0.0:"+strconv.FormatUint(globals.ServerPort, 10))
	if err != nil {
		return nil, err
	}
	sock, err := net.ListenUDP("udp", addr)
	if err != nil {
		return nil, err
	}
	l.sock = sock
	l.running = true
	go l.run()
	return l, nil
}

func (l *Listener) Close() {
	l.running = false
	for !l.finished {
		time.Sleep(1 * time.Millisecond)
	}
	_ = l.sock.Close()
}

func (l *Listener) read() {
	_ = l.sock.SetReadDeadline(time.Now().Add(500 * time.Millisecond))
	data := make([]byte, format.MaxPacketSize)
	dataAvailable, addr, err := l.sock.ReadFromUDP(data)
	if dataAvailable > 0 && addr != nil && err == nil {
		parsed, err := format.Parse(data[:dataAvailable], addr)
		if err != nil {
			return
		}
		if len(parsed.Data) < 2 {
			return
		}
		action, err := strconv.ParseUint(string(parsed.Data[0]), 10, 32)
		if err != nil {
			return
		}
		if action != format.Hello {
			return
		}
		globals.Clients.Add(addr.IP.String(), string(parsed.Data[1]))
		hostname, err := os.Hostname()
		if err != nil {
			hostname = " "
		}
		sendHello(addr, hostname)
	} else if err != nil && !err.(net.Error).Timeout() {
		l.err = err
	}
}

func (l *Listener) run() {
	for l.running {
		l.read()
		time.Sleep(1 * time.Millisecond)
	}
	l.finished = true
}
