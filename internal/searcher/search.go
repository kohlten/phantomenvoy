package searcher

import (
	"fmt"
	"gitlab.com/kohlten/phantomenvoy/internal/packet/format"
	"net"
	"os"
	"strconv"
	"strings"

	"gitlab.com/kohlten/phantomenvoy/internal/globals"
)

const (
    maxPort = 65535
    minPort = 1
)

func isIPv4(address string) bool {
    return strings.Count(address, ":") < 2
}

func sendHello(addr *net.UDPAddr, hostname string) {
    sock, err := net.DialUDP("udp", nil, addr)
    if err != nil {
        return
    }
    data, err := format.Format(format.MaxSearchPacketSize, format.Hello, hostname)
    if err != nil {
        fmt.Println("failed to format search packet with error:", err.Error())
    }
    _, err = sock.Write(data)
    if err != nil {
        return
    }
    _ = sock.Close()
}

func Search() error {
    interfaceAddrs, err := net.InterfaceAddrs()
    if err != nil {
        return err
    }
    hostname, err := os.Hostname()
    if err != nil {
        hostname = " "
    }
    for i := 0; i < len(interfaceAddrs); i++ {
        addr := interfaceAddrs[i]
        addrStr := addr.String()
        if !isIPv4(addrStr) {
            continue
        }
        for k := len(addrStr) - 1; k >= 0; k-- {
            if addrStr[k] == '/' {
                addrStr = addrStr[:k]
                break
            }
        }
        ip := net.ParseIP(addrStr)
        if ip == nil {
            continue
        }
        for i := 0; i < 255; i++ {
            ipStr := ip.String()
            for j := len(ipStr) - 1; j >= 0; j-- {
                if ipStr[j] == '.' {
                    break
                }
                ipStr = ipStr[0:j]
            }
            ipStr = ipStr + strconv.FormatUint(uint64(i), 10)
            udpAddr, err := net.ResolveUDPAddr("udp", ipStr+":"+strconv.FormatUint(globals.ServerPort, 10))
            if err != nil {
                continue
            }
            sendHello(udpAddr, hostname)
        }
    }
    return nil
}
