package globals

import (
    "gitlab.com/kohlten/phantomenvoy/internal/clientBank"
    "gitlab.com/kohlten/phantomenvoy/internal/events"
    "log"
    "path/filepath"
    "runtime"
    "strings"
)

const (
    ServerPort = 8181
)

var Events *events.EventManager
var Clients = clientBank.NewClientBank()
var BaseDir string

func init() {
    _, base, _, ok := runtime.Caller(0)
    if !ok  {
        log.Fatalln("Failed to get package path")

    }
    split := strings.Split(filepath.Dir(base), string(filepath.Separator))
    split = split[:len(split) - 2]
    BaseDir = string(filepath.Separator) + filepath.Join(split...)
}
