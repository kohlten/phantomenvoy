package fileIndexer

import (
	"errors"
	"fmt"
	"io"
	"os"
)

var errorOutOfBounds = errors.New("out of bounds")

// FileIndexer stores the section start locations for easy lookup.
type FileIndexer struct {
	chunkSize   uint
	chunkLength uint
	length      uint
	index       map[uint]uint
	fd          *os.File
}

func NewFileIndexer(chunkSize uint, filename string) (*FileIndexer, error) {
	indexer := new(FileIndexer)
	indexer.chunkSize = chunkSize
	indexer.index = make(map[uint]uint)
	err := indexer.indexFile(filename)
	if err != nil {
		return nil, err
	}
	return indexer, nil
}

func (index *FileIndexer) indexFile(filename string) error {
	fd, err := os.Open(filename)
	if err != nil {
		return err
	}
	length, err := fd.Seek(0, io.SeekEnd)
	if err != nil {
		return err
	}
	if length == 0 {
		return fmt.Errorf("file %s is empty", filename)
	}
	_, err = fd.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}
	index.length = uint(length)
	index.fd = fd
	index.chunkLength = index.length / index.chunkSize
	// If the file has extra data on the end, need one more chunk for that data
	if index.length%index.chunkSize != 0 {
		index.chunkLength++
	}
	// Make sure we always have at least one chunk (@TODO Is this needed?)
	if index.chunkLength == 0 && length > 0 {
		index.chunkLength = 1
		index.index[0] = 0
	} else {
		for i := uint(0); i <= index.chunkLength; i++ {
			index.index[i] = i * index.chunkSize
		}
	}
	return nil
}

// Returns the data in the section with the index of loc
func (index *FileIndexer) Get(loc uint) ([]byte, error) {
	offset, ok := index.index[loc]
	if !ok {
		return nil, errorOutOfBounds
	}
	_, err := index.fd.Seek(int64(offset), io.SeekStart)
	if err != nil {
		return nil, err
	}
	data := make([]byte, index.chunkSize)
	bytes, err := index.fd.Read(data)
	if err != nil {
		return nil, err
	}
	_, err = index.fd.Seek(0, io.SeekStart)
	if err != nil {
		return nil, err
	}
	return data[:bytes], nil
}

func (index *FileIndexer) GetLength() uint {
	return index.length
}

func (index *FileIndexer) GetChunkLength() uint {
	return index.chunkLength
}

func (index *FileIndexer) Close() {
	_ = index.fd.Close()
}
