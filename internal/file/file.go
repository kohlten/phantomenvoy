package file

import (
	"gitlab.com/kohlten/phantomenvoy/internal/fileIndexer"
	"gitlab.com/kohlten/phantomenvoy/internal/tmpWriter"
)

type File struct {
	Client           uint
	Tmp              *tmpWriter.TmpWriter
	Indexer          *fileIndexer.FileIndexer
	Sent             uint   // How many chunks have been sent
	Size             uint   // Total size of the file
	Name             string // File name
	Hash             string // Hash of the whole file
	ChunksLen        uint   // How many chunks
	Tries            uint   // How many times it has failed to send a section
	LastUpdate       uint   // Last time a packet was received for this file
	Id               string // UUID of this file
	Allowed          bool   // If it has been accepted or not
	ReceivedShutdown bool   // If the client end has shutdown or not
	Denied           bool
}

type CompletedFile struct {
	Size       uint
	Name       string
	Hash       string
	OutputName string // Where the file is currently located
}

type Info struct {
	Id          string
	Name        string
	Size        uint
	Hash        string
	TotalChunks uint
	Chunks      uint // How many chunks have been sent
}
