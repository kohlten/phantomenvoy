package format

import (
	"errors"
    "fmt"
    "reflect"
    "strconv"
)

const (
    Hello = 0x1
    Disconnect = 0x2

    maxValueSize = 99999999
    maxValueStrLen = 8

    MaxPacketSize = 20480
    MaxSearchPacketSize = 80
)

func parseValue(value interface{}) []byte {
    valueStr := make([]byte, 0)
    switch v := value.(type) {
    case int, int8, int16, int32, int64:
        valueStr = []byte(strconv.FormatInt(reflect.ValueOf(v).Int(), 10))
    case uint, uint8, uint16, uint32, uint64:
        valueStr = []byte(strconv.FormatUint(reflect.ValueOf(v).Uint(), 10))
    case float32:
        valueStr = []byte(strconv.FormatFloat(reflect.ValueOf(v).Float(), 'f', 5, 32))
    case float64:
        valueStr = []byte(strconv.FormatFloat(reflect.ValueOf(v).Float(), 'f', 5, 64))
    case string:
        valueStr = []byte(reflect.ValueOf(v).String())
    case []byte:
        valueStr = value.([]byte)
    case bool:
        valueStr = []byte(strconv.FormatBool(reflect.ValueOf(v).Bool()))
    }
    return valueStr
}

func createValueLenStr(valueLen uint) string {
    valueLenStr := strconv.FormatUint(uint64(valueLen), 10)
    for len(valueLenStr) < maxValueStrLen {
        valueLenStr = "0" + valueLenStr
    }
    return valueLenStr
}

// Max size of a value is 99,999,999. This is arbitrary, it could be larger or smaller.
// valueSize value
// 00000001   1
func Format(maxSize int, values ...interface{}) ([]byte, error) {
    parsed := make([]byte, 0)
    for i := 0; i < len(values); i++ {
        value := parseValue(values[i])
        // Consider making this an error
        if len(value) == 0 {
            continue
        }
        if len(value) > maxValueSize {
            return nil, errors.New(fmt.Sprint("value length of", len(value), "is greater than the max size"))
        }
        valueLenStr := createValueLenStr(uint(len(value)))
        parsed = append(parsed, valueLenStr...)
        parsed = append(parsed, value...)
        if len(parsed) > maxSize {
            return nil, errors.New("packet is too long")
        }
    }
    if len(parsed) < maxSize {
        valueLen := (maxSize - len(parsed)) - maxValueStrLen
        if valueLen < 0 {
            return nil, errors.New("not enough room to put zeros")
        }
        valueLenStr := createValueLenStr(uint(valueLen))
        parsed = append(parsed, valueLenStr...)
        for len(parsed) < maxSize {
            parsed = append(parsed, 0)
        }
    }
    return parsed, nil
}
