package format

import (
    "errors"
    "net"
    "strconv"
)

type Parsed struct {
    Data [][]byte
    Addr *net.UDPAddr
}

var (
    errorNotEnoughData     = errors.New("not enough data in the input array")
    errorValueLenNotParsed = errors.New("value length was invalid")
)

func Parse(data []byte, addr *net.UDPAddr) (*Parsed, error) {
    parsed := new(Parsed)
    parsed.Addr = addr
    i := uint64(0)
    for i < uint64(len(data)) {
        if uint64(len(data))-i < maxValueStrLen {
            return nil, errorNotEnoughData
        }
        valueLenStr := data[i : i+maxValueStrLen]
        valueLen, err := strconv.ParseUint(string(valueLenStr), 10, 64)
        if err != nil {
            return nil, errorValueLenNotParsed
        }
        i += maxValueStrLen
        if valueLen > maxValueSize {
            return nil, errorValueLenNotParsed
        }
        if uint64(len(data))-i < valueLen {
            return nil, errorNotEnoughData
        }
        value := data[i : i+valueLen]
        parsed.Data = append(parsed.Data, value)
        i += valueLen
    }
    return parsed, nil
}
