package packet

import (
	"errors"
	"gitlab.com/kohlten/phantomenvoy/internal/events"
	"gitlab.com/kohlten/phantomenvoy/internal/file"
	"gitlab.com/kohlten/protocol/protocol"
	"gitlab.com/kohlten/protocol/util"
)

const (
	RequestToSend = uint16(0x0)
	AcceptRequest = uint16(0x1)
	DenyRequest   = uint16(0x2)
	Data          = uint16(0x3)
	ResendData    = uint16(0x4)
	Failed        = uint16(0x5)
	Ok            = uint16(0x6)
)

var (
	failedToParse = errors.New("failed to parse the packet")
)

// @TODO Possibly return an error value here
func SendRequestToSend(sock util.Conn, clientId uint, f *file.File) {
	_ = sock.Send(clientId, util.WrapValues("action", RequestToSend, "name", f.Name, "id", f.Id, "size", f.Size, "chunks", f.ChunksLen, "hash", f.Hash))
}

func SendAcceptRequest(sock util.Conn, clientId uint, id string) {
	_ = sock.Send(clientId, util.WrapValues("action", AcceptRequest, "id", id))
}

func SendDenyRequest(sock util.Conn, clientId uint, id string) {
	_ = sock.Send(clientId, util.WrapValues("action", DenyRequest, "id", id))
}

func SendData(sock util.Conn, clientId uint, id, hash string, index uint, data []byte) {
	_ = sock.Send(clientId, util.WrapValues("action", Data, "id", id, "index", index, "hash", hash, "data", data))
}

func SendResendData(sock util.Conn, clientId uint, id string, index uint, result bool) {
	_ = sock.Send(clientId, util.WrapValues("action", ResendData, "id", id, "index", index, "result", result))
}

func SendFailed(sock util.Conn, clientId uint, id string) {
	_ = sock.Send(clientId, util.WrapValues("action", Failed, "id", id))
}

func SendOk(sock util.Conn, clientId uint, id string) {
	_ = sock.Send(clientId, util.WrapValues("action", Ok, "id", id))
}

func ParsePacket(data map[string]interface{}, client uint) (events.Event, error) {
	actionInterface, ok := data["action"]
	if !ok || !protocol.IsType(actionInterface, protocol.TypeUInt) {
		return nil, failedToParse
	}
	action := actionInterface.(uint16)
	switch action {
	case RequestToSend:
		return parseRequestToSend(data, client)
	case AcceptRequest, DenyRequest:
		return parseRequest(data, action)
	case Data:
		return parseData(data, client)
	case ResendData:
		return parseResend(data, client)
	case Failed, Ok:
		return parseSendStatus(data, client, action)
	}
	return nil, failedToParse
}

func parseRequestToSend(data map[string]interface{}, client uint) (events.Event, error) {
	if len(data) < 6 {
		return nil, failedToParse
	}
	nameInterface, ok := data["name"]
	if !ok || !protocol.IsType(nameInterface, protocol.TypeString) {
		return nil, failedToParse
	}
	name := nameInterface.(string)
	idInterface, ok := data["id"]
	if !ok || !protocol.IsType(idInterface, protocol.TypeString) {
		return nil, failedToParse
	}
	id := idInterface.(string)
	fileSizeInterface, ok := data["size"]
	if !ok || !protocol.IsType(fileSizeInterface, protocol.TypeUInt) {
		return nil, failedToParse
	}
	fileSize := fileSizeInterface.(uint64)
	chunksInterface, ok := data["chunks"]
	if !ok || !protocol.IsType(chunksInterface, protocol.TypeUInt) {
		return nil, failedToParse
	}
	chunks := chunksInterface.(uint64)
	hashInterface, ok := data["hash"]
	if !ok || !protocol.IsType(hashInterface, protocol.TypeString) {
		return nil, failedToParse
	}
	hash := hashInterface.(string)
	event := &events.RequestToSend{
		Client:   client,
		Id:       id,
		Filename: name,
		FileSize: uint(fileSize),
		Chunks:   uint(chunks),
		Hash:     hash,
	}
	return event, nil
}

func parseRequest(data map[string]interface{}, action uint16) (events.Event, error) {
	if len(data) > 3 {
		return nil, failedToParse
	}
	result := false
	if action == AcceptRequest {
		result = true
	} else if action == DenyRequest {
		result = false
	} else {
		return nil, failedToParse
	}
	idInterface, ok := data["id"]
	if !ok || !protocol.IsType(idInterface, protocol.TypeString) {
		return nil, failedToParse
	}
	id := idInterface.(string)
	event := &events.RequestResult{
		Result: result,
		Id:     id,
	}
	return event, nil
}

func parseData(data map[string]interface{}, client uint) (events.Event, error) {
	if len(data) < 5 {
		return nil, failedToParse
	}
	idInterface, ok := data["id"]
	if !ok || !protocol.IsType(idInterface, protocol.TypeString) {
		return nil, failedToParse
	}
	id := idInterface.(string)
	indexInterface, ok := data["index"]
	if !ok || !protocol.IsType(indexInterface, protocol.TypeUInt) {
		return nil, failedToParse
	}
	index := indexInterface.(uint64)
	hashInterface, ok := data["hash"]
	if !ok || !protocol.IsType(hashInterface, protocol.TypeString) {
		return nil, failedToParse
	}
	hash := hashInterface.(string)
	dataInterface, ok := data["data"]
	if !ok || !protocol.IsType(dataInterface, protocol.TypeBytes) {
		return nil, failedToParse
	}
	bytes := dataInterface.([]byte)
	event := &events.Data{
		Client: client,
		Id:     id,
		Index:  uint(index),
		Hash:   hash,
		Data:   bytes,
	}
	return event, nil
}

func parseResend(data map[string]interface{}, client uint) (events.Event, error) {
	if len(data) < 4 {
		return nil, failedToParse
	}
	idInterface, ok := data["id"]
	if !ok || !protocol.IsType(idInterface, protocol.TypeString) {
		return nil, failedToParse
	}
	id := idInterface.(string)
	indexInterface, ok := data["index"]
	if !ok || !protocol.IsType(indexInterface, protocol.TypeUInt) {
		return nil, failedToParse
	}
	index := indexInterface.(uint64)
	resultInterface, ok := data["result"]
	if !ok || !protocol.IsType(resultInterface, protocol.TypeBool) {
		return nil, failedToParse
	}
	result := resultInterface.(bool)
	event := &events.Resend{
		Client: client,
		Id:     id,
		Index:  uint(index),
		Result: result,
	}
	return event, nil
}

func parseSendStatus(data map[string]interface{}, client uint, action uint16) (events.Event, error) {
	if len(data) > 3 {
		return nil, failedToParse
	}
	result := false
	if action == Ok {
		result = true
	} else if action == Failed {
		result = false
	} else {
		return nil, failedToParse
	}
	idInterface, ok := data["id"]
	if !ok || !protocol.IsType(idInterface, protocol.TypeString) {
		return nil, failedToParse
	}
	id := idInterface.(string)
	event := &events.SendResult{
		Client: client,
		Id:     id,
		Result: result,
	}
	return event, nil
}
