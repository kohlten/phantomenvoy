package receive

import (
	"crypto/tls"
	"errors"
	phantomEvents "gitlab.com/kohlten/phantomenvoy/internal/events"
	"gitlab.com/kohlten/phantomenvoy/internal/file"
	"gitlab.com/kohlten/phantomenvoy/internal/globals"
	protocolEvents "gitlab.com/kohlten/protocol/events"
	"gitlab.com/kohlten/protocol/protocol"
	"gitlab.com/kohlten/protocol/server"
	"os"
	"strconv"
	"sync"
	"time"
)

// Server for receiving files
type Receiver struct {
	sock        *server.Server
	clients     []*rClient
	clientMutex *sync.Mutex
	running     bool
	finished    bool
}

var errorAlreadyIn = errors.New("file already exists")
var errorNotFound = errors.New("file was not found")

func NewReceiver() (*Receiver, error) {
	r := new(Receiver)
	// Load the certificate generated from the build
	cert, err := tls.LoadX509KeyPair(globals.BaseDir+"/certs/server.crt", globals.BaseDir+"/certs/server.key")
	if err != nil {
		return nil, err
	}
	config := tls.Config{Certificates: []tls.Certificate{cert}}
	// Create a new server using the certificate on the port 8181
	sock, err := tls.Listen("tcp", "0.0.0.0:"+strconv.FormatUint(globals.ServerPort, 10), &config)
	if err != nil {
		return nil, err
	}
	r.sock = server.NewServer(sock, nil)
	r.running = true
	r.clientMutex = &sync.Mutex{}
	protocol.SetMTU(2048)
	//go r.accept()
	go r.update()
	return r, nil
}

// Returns the files that are finished for all clients
func (r *Receiver) GetFinishedFileInfo() []*file.Info {
	r.clientMutex.Lock()
	defer r.clientMutex.Unlock()
	files := make([]*file.Info, 0)
	for i := 0; i < len(r.clients); i++ {
		cFiles := r.clients[i].getFinishedFileInfo()
		files = append(files, cFiles...)
	}
	return files
}

/// Returns the file info for the files that are currently being processed
func (r *Receiver) GetProcessingFileInfo() []*file.Info {
	r.clientMutex.Lock()
	defer r.clientMutex.Unlock()
	files := make([]*file.Info, 0)
	for i := 0; i < len(r.clients); i++ {
		cFiles := r.clients[i].getProcessingFileInfo()
		files = append(files, cFiles...)
	}
	return files
}

// Returns the completed file with ID. If no file is found, it will return an error.
func (r *Receiver) GetFile(id string) (*file.CompletedFile, error) {
	for i := 0; i < len(r.clients); i++ {
		f := r.clients[i].get(id)
		if f != nil {
			err := f.Tmp.CombineAndMove(os.TempDir(), f.Id+".data")
			if err != nil {
				return nil, err
			}
			f.Tmp.Close()
			cFile := &file.CompletedFile{
				Size:       f.Size,
				Name:       f.Name,
				Hash:       f.Hash,
				OutputName: os.TempDir() + "/" + f.Id + ".data",
			}
			return cFile, nil
		}
	}
	return nil, errorNotFound
}

// Tries to cancel a file currently being sent using the ID
func (r *Receiver) Cancel(id string) {
	r.clientMutex.Lock()
	defer r.clientMutex.Unlock()
	for _, client := range r.clients {
		if client.hasFile(id) {
			client.cancel(id)
			return
		}
	}
}

// Closes the receiver and all open clients
func (r *Receiver) Close() {
	r.running = false
	for !r.finished {
		time.Sleep(1 * time.Millisecond)
	}
	r.clientMutex.Lock()
	for _, client := range r.clients {
		client.close()
	}
	r.clientMutex.Unlock()
	r.sock.Close()
}

func (r *Receiver) update() {
	for r.running {
		r.updateEvents()
		r.updateClients()
		time.Sleep(1 * time.Millisecond)
	}
	r.finished = true
}

func (r *Receiver) updateClients() {
	r.clientMutex.Lock()
	defer r.clientMutex.Unlock()
	for i := 0; i < len(r.clients); {
		if r.clients[i].finished {
			r.clients = append(r.clients[:i], r.clients[i+1:]...)
		} else {
			i++
		}
	}
}

func (r *Receiver) findFileClient(id string) *rClient {
	r.clientMutex.Lock()
	defer r.clientMutex.Unlock()
	for _, client := range r.clients {
		if client.hasFile(id) {
			return client
		}
	}
	return nil
}

func (r *Receiver) findClientById(id uint) *rClient {
	r.clientMutex.Lock()
	defer r.clientMutex.Unlock()
	for _, client := range r.clients {
		if client.getId() == id {
			return client
		}
	}
	return nil
}

func (r *Receiver) updateEvents() {
	for event := r.sock.Events.Poll(); event != nil; event = r.sock.Events.Poll() {
		switch e := event.(type) {
		case *protocolEvents.ReceivedPacketEvent:
			client := r.findClientById(e.Id)
			if client != nil {
				client.addPacket(e.Packet)
			}
		case *protocolEvents.NewClientEvent:
			client := newRClient(r.sock, e.Id)
			r.clientMutex.Lock()
			r.clients = append(r.clients, client)
			r.clientMutex.Unlock()
		/*case *protocolEvents.ClientDisconnectEvent:
			client := r.findClientById(e.Id)
			if client != nil {
				client.close()
			}*/
		}
	}
	for i := uint(0); i < globals.Events.Len(); {
		event := globals.Events.Get(i)
		switch e := event.(type) {
		case *phantomEvents.RequestResult:
			client := r.findFileClient(e.Id)
			if client == nil {
				return
			}
			client.setFileStatus(e)
			globals.Events.Remove(i)
		default:
			i++
		}
	}
}
