package receive

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"gitlab.com/kohlten/phantomenvoy/internal/events"
	"gitlab.com/kohlten/phantomenvoy/internal/file"
	"gitlab.com/kohlten/phantomenvoy/internal/globals"
	"gitlab.com/kohlten/phantomenvoy/internal/packet"
	"gitlab.com/kohlten/phantomenvoy/internal/send"
	"gitlab.com/kohlten/phantomenvoy/internal/tmpWriter"
	"gitlab.com/kohlten/protocol/server"
	"sync"
	"time"
)

const (
	maxWaitTime = 300000 // Max wait time of 5 minutes. The Receiver will exit after that time is up.
	maxTries    = uint(5)
	PacketsPer  = 32
)

type rClient struct {
	files         map[string]*file.File // Files being processed
	finishedFiles map[string]*file.File // Files that are completed
	sock          *server.Server
	id            uint
	parsedMutex   *sync.Mutex
	packetMutex   *sync.Mutex
	fileMutex     *sync.Mutex
	running       bool
	finished      bool
	hasHadFile    bool
	toBeParsed    []map[string]interface{}
	parsed        []*events.Event
}

func newRClient(sock *server.Server, id uint) *rClient {
	c := new(rClient)
	c.files = make(map[string]*file.File)
	c.finishedFiles = make(map[string]*file.File)
	c.sock = sock
	c.id = id
	c.parsedMutex = &sync.Mutex{}
	c.packetMutex = &sync.Mutex{}
	c.fileMutex = &sync.Mutex{}
	c.running = true
	c.finished = false
	c.toBeParsed = make([]map[string]interface{}, 0)
	go c.run()
	return c
}

func (c *rClient) getId() uint {
	return c.id
}

func (c *rClient) addPacket(packet map[string]interface{}) {
	c.packetMutex.Lock()
	defer c.packetMutex.Unlock()
	c.toBeParsed = append(c.toBeParsed, packet)
}

func (c *rClient) run() {
	for c.running {
		c.parsePackets()
		c.doPacketActions()
		c.updateFiles()
		time.Sleep(1 * time.Millisecond)
	}
	c.finished = true
}

func (c *rClient) parsePackets() {
	c.packetMutex.Lock()
	defer c.packetMutex.Unlock()
	for i := 0; i < len(c.toBeParsed); i++ {
		parsed, err := packet.ParsePacket(c.toBeParsed[i], c.id)
		if err != nil {
			c.close()
			return
		}
		c.parsedMutex.Lock()
		c.parsed = append(c.parsed, &parsed)
		c.parsedMutex.Unlock()
	}
	c.toBeParsed = nil
}

func (c *rClient) doPacketActions() {
	c.parsedMutex.Lock()
	defer c.parsedMutex.Unlock()
	for _, event := range c.parsed {
		switch e := (*event).(type) {
		case *events.Data:
			c.addData(e)
		case *events.SendResult:
			if e.Result == false {
				f := c.files[e.Id]
				if f != nil {
					c.files[e.Id].ReceivedShutdown = true
				}
			}
		case *events.RequestToSend:
			err := c.addFile(e)
			if err != nil {
				packet.SendFailed(c.sock, e.Client, e.Id)
				globals.Events.Push(&events.SendResult{
					Client:   e.Client,
					Id:       e.Id,
					Filename: e.Filename,
					Result:   false,
				})
				c.close()
				return
			}
			globals.Events.Push(e)
		}
	}
	c.parsed = nil
}

func (c *rClient) updateFiles() {
	c.fileMutex.Lock()
	currentTime := uint(time.Now().UnixNano() / int64(time.Millisecond))
	for _, f := range c.files {
		if f.Tries >= maxTries || currentTime-f.LastUpdate >= maxWaitTime {
			packet.SendFailed(c.sock, f.Client, f.Id)
			globals.Events.Push(&events.SendResult{
				Client:   c.id,
				Id:       f.Id,
				Filename: f.Name,
				Result:   false,
			})
			delete(c.files, f.Id)
		}
		if f.Denied && f.ReceivedShutdown {
			delete(c.files, f.Id)
		}
	}
	c.fileMutex.Unlock()
	if len(c.files) == 0 && len(c.finishedFiles) == 0 && c.hasHadFile {
		c.close()
	}
}

func (c *rClient) addFile(event *events.RequestToSend) error {
	c.fileMutex.Lock()
	defer c.fileMutex.Unlock()
	f := new(file.File)
	f.Client = event.Client
	f.Name = event.Filename
	f.Size = event.FileSize
	f.ChunksLen = event.Chunks
	f.Hash = event.Hash
	f.Id = event.Id
	tmp, err := tmpWriter.NewTmpWriter("Receiver", f.Id)
	if err != nil {
		return err
	}
	f.Tmp = tmp
	event.Id = f.Id
	event.Processed = true
	if _, ok := c.files[f.Id]; ok {
		return errorAlreadyIn
	}
	f.LastUpdate = uint(time.Now().UnixNano() / int64(time.Millisecond))
	c.files[f.Id] = f
	c.hasHadFile = true
	return nil
}

func (c *rClient) addData(event *events.Data) {
	c.fileMutex.Lock()
	defer c.fileMutex.Unlock()
	f, ok := c.files[event.Id]
	if !ok {
		fmt.Println("Client tried to Send a file without requesting!")
		return
	}
	hash := sha256.Sum256(event.Data)
	if hex.EncodeToString(hash[:]) != event.Hash {
		packet.SendResendData(c.sock, event.Client, f.Id, event.Index, true)
		f.Tries++
		return
	}
	if event.Index%uint(send.PacketsPer-1) == 0 && event.Index > 0 {
		packet.SendResendData(c.sock, event.Client, f.Id, event.Index, false)
	}
	err := f.Tmp.Add(event.Hash, event.Id, event.Data, event.Index, uint(len(event.Data)))
	if err != nil {
		packet.SendFailed(c.sock, f.Client, f.Id)
		globals.Events.Push(&events.SendResult{
			Client:   c.id,
			Id:       f.Id,
			Filename: f.Name,
			Result:   false,
		})
		delete(c.files, f.Id)
		return
	}
	if f.Tmp.GetPartsLen() >= f.ChunksLen {
		c.finishFile(f)
	}
	f.LastUpdate = uint(time.Now().UnixNano() / int64(time.Millisecond))
}

func (c *rClient) finishFile(f *file.File) {
	packet.SendOk(c.sock, f.Client, f.Id)
	globals.Events.Push(&events.SendResult{
		Client:   c.id,
		Id:       f.Id,
		Filename: f.Name,
		Result:   true,
	})
	delete(c.files, f.Id)
	c.finishedFiles[f.Id] = f
}

func (c *rClient) hasFile(id string) bool {
	c.fileMutex.Lock()
	defer c.fileMutex.Unlock()
	_, ok := c.files[id]
	if !ok {
		_, ok = c.finishedFiles[id]
	}
	return ok
}

func (c *rClient) get(id string) *file.File {
	c.fileMutex.Lock()
	defer c.fileMutex.Unlock()
	f, ok := c.finishedFiles[id]
	if !ok {
		return nil
	}
	delete(c.finishedFiles, id)
	return f
}

func (c *rClient) setFileStatus(request *events.RequestResult) {
	if request.Result {
		for _, f := range c.files {
			if f.Id == request.Id {
				f.Allowed = true
				break
			}
		}
		packet.SendAcceptRequest(c.sock, c.id, request.Id)
	} else {
		packet.SendDenyRequest(c.sock, c.id, request.Id)
		c.files[request.Id].Denied = true
	}
}

func (c *rClient) getFinishedFileInfo() []*file.Info {
	return c.getFileInfo(c.finishedFiles)
}

func (c *rClient) getProcessingFileInfo() []*file.Info {
	return c.getFileInfo(c.files)
}

func (c *rClient) getFileInfo(dict map[string]*file.File) []*file.Info {
	c.fileMutex.Lock()
	defer c.fileMutex.Unlock()
	files := make([]*file.Info, 0)
	for _, v := range dict {
		f := &file.Info{
			Id:          v.Id,
			Size:        v.Size,
			Name:        v.Name,
			Hash:        v.Hash,
			Chunks:      v.Tmp.GetPartsLen(),
			TotalChunks: v.ChunksLen,
		}
		files = append(files, f)
	}
	return files
}

func (c *rClient) cancel(id string) {
	c.fileMutex.Lock()
	defer c.fileMutex.Unlock()
	for _, f := range c.files {
		if f.Id == id {
			packet.SendFailed(c.sock, f.Client, f.Id)
			globals.Events.Push(&events.SendResult{
				Client:   c.id,
				Id:       f.Id,
				Filename: f.Name,
				Result:   false,
			})
			f.Tmp.Close()
			delete(c.files, f.Id)
		}
	}
}

func (c *rClient) close() {
	for key, _ := range c.files {
		c.cancel(key)
	}
	for key, _ := range c.finishedFiles {
		c.cancel(key)
	}
	c.running = false
	c.files = nil
	c.finishedFiles = nil
}
