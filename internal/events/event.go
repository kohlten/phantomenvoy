package events

type Event interface{}

type RequestToSend struct {
	Client    uint
	Filename  string
	FileSize  uint
	Chunks    uint
	Hash      string
	Id        string
	Processed bool
}

type RequestResult struct {
	Client   uint
	Id       string
	Filename string
	Result   bool // If the request was accepted or not
}

type SendResult struct {
	Client   uint
	Id       string
	Filename string
	Result   bool // If the file was completed or not
}

// @TODO Possibly rename due to confusion on purpose
// This is sent as an acknowledgement of a section of a file. If the section was added successfully, the Result will be false.
// Otherwise, the result will be true and this section will be resent.
type Resend struct {
	Client   uint
	Id       string
	Filename string
	Index    uint
	Result   bool // Whether to resend or not
}

// Section of a file
type Data struct {
	Client   uint
	Id       string
	Filename string
	Index    uint
	Hash     string
	Data     []byte
}

type Error struct {
	Ip     string
	Reason string
}
