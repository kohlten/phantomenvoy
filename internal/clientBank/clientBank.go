package clientBank

import "sync"

type Client struct {
	Ip string
	Name string
}

// ClientBank allows storing of the ip and hostname of clients found to be used for easy lookup
type ClientBank struct {
	clients []*Client
	mutex *sync.Mutex
}

func newClient(ip, name string) *Client {
	return &Client{Ip: ip, Name: name}
}

func NewClientBank() *ClientBank {
	return &ClientBank{clients: nil, mutex: &sync.Mutex{}}
}

// Returns true if it was successfully added. False if the hostname was already added.
func (c *ClientBank) Add(ip, name string) bool {
	if c.IsInIp(ip) {
		return false
	}
	client := newClient(ip, name)
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.clients = append(c.clients, client)
	return true
}

// Returns if an ip is in the bank
func (c *ClientBank) IsInIp(ip string) bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	for i := 0; i < len(c.clients); i++ {
		if c.clients[i].Ip == ip {
			return true
		}
	}
	return false
}

// Returns if a hostname is in the bank
func (c *ClientBank) IsInName(name string) bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	for i := 0; i < len(c.clients); i++ {
		if c.clients[i].Name == name {
			return true
		}
	}
	return false
}

// Returns if a client (Ip and Hostname) is in the bank
func (c *ClientBank) IsIn(client *Client) bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	for i := 0; i < len(c.clients); i++ {
		if c.clients[i].Name == client.Name && c.clients[i].Ip == client.Ip {
			return true
		}
	}
	return false
}

// Removes a client from the bank by IP
func (c *ClientBank) RemoveIp(ip string) bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	for i := 0; i < len(c.clients); i++ {
		if c.clients[i].Ip == ip {
			c.clients = append(c.clients[:i], c.clients[i + 1:]...)
			return true
		}
	}
	return false
}

// Removes a client from the bank by hostname
func (c *ClientBank) RemoveName(name string) bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	for i := 0; i < len(c.clients); i++ {
		if c.clients[i].Name == name {
			c.clients = append(c.clients[:i], c.clients[i + 1:]...)
			return true
		}
	}
	return false
}

// Removes a client based off of the IP and hostname
func (c *ClientBank) RemoveClient(client *Client) bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	for i := 0; i < len(c.clients); i++ {
		if c.clients[i].Name == client.Name && c.clients[i].Ip == client.Ip {
			c.clients = append(c.clients[:i], c.clients[i + 1:]...)
			return true
		}
	}
	return false
}

// Returns a client in the bank based off of IP
func (c *ClientBank) GetIP(ip string) *Client {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	for i := 0; i < len(c.clients); i++ {
		if c.clients[i].Ip == ip {
			return c.clients[i]
		}
	}
	return nil
}

// Returns a client in the bank based off of hostname
func (c *ClientBank) GetName(name string) *Client {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	for i := 0; i < len(c.clients); i++ {
		if c.clients[i].Name == name {
			return c.clients[i]
		}
	}
	return nil
}

// Returns a copy of all the internal clients
func (c *ClientBank) GetAll() []*Client {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	clients := make([]*Client, len(c.clients))
	for i := 0; i < len(c.clients); i++ {
		clients[i] = newClient(c.clients[i].Ip, c.clients[i].Name)
	}
	return clients
}

// Removes all clients from the bank
func (c *ClientBank) Clear() {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.clients = nil
}