package tmpWriter

import (
	"bytes"
	"compress/zlib"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"io"
	"os"
	"sort"
	"strconv"
	"sync"
)

var tempDir = os.TempDir()

var (
	errorInvalidId     = errors.New("invalid id")
	errorAlreadyExists = errors.New("index already exists")
	errorDoesNotExists = errors.New("index does not exist")
	errorCorrupted     = errors.New("part has been corrupted")
)

type Part struct {
	Hash           string
	Size           uint
	compressedSize uint
	Index          uint
	name           string // Name of file that stores this part
}

type TmpWriter struct {
	id     string
	prefix string // Name to put as prefix for tmp directory
	dir    string // Location of tmp directory
	parts  []*Part
	lock   sync.Mutex
}

func NewTmpWriter(prefix, id string) (*TmpWriter, error) {
	dirname := tempDir + "/" + prefix + "_" + id
	err := os.Mkdir(dirname, 0755)
	if err != nil {
		return nil, err
	}
	tmpWriter := new(TmpWriter)
	tmpWriter.dir = dirname
	tmpWriter.prefix = prefix
	tmpWriter.id = id
	return tmpWriter, nil
}

// Adds a part to the temp directory
func (t *TmpWriter) Add(hash, id string, data []byte, index, size uint) error {
	t.lock.Lock()
	defer t.lock.Unlock()
	if id != t.id {
		return errorInvalidId
	}
	// Find if the part was already added.
	for i := 0; i < len(t.parts); i++ {
		if t.parts[i].Index == index {
			return errorAlreadyExists
		}
	}
	// Create and open the temp file that stores that part
	fileName := tempDir + "/" + t.prefix + "_" + t.id + "/" + strconv.FormatUint(uint64(index), 10)
	file, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		return err
	}
	// Compress the data
	buffer := bytes.NewBuffer(nil)
	compressor := zlib.NewWriter(buffer)
	_, _ = compressor.Write(data)
	err = compressor.Close()
	if err != nil {
		_ = file.Close()
		_ = os.Remove(fileName)
		return err
	}
	// Write the compressed data to the temp file
	compressed := buffer.Bytes()
	_, err = file.Write(compressed)
	if err != nil {
		_ = file.Close()
		_ = os.Remove(fileName)
		return err
	}
	// Add the part to the internal list
	t.parts = append(t.parts, &Part{Hash: hash, Size: size, compressedSize: uint(len(compressed)), name: fileName, Index: index})
	return nil
}

// Gets the data of a part
func (t *TmpWriter) Get(id string, index uint) ([]byte, *Part, error) {
	if id != t.id {
		return nil, nil, errorInvalidId
	}
	part := t.parts[0]
	found := false
	// Find the part
	for i := 0; i < len(t.parts); i++ {
		if t.parts[i].Index == index {
			found = true
			part = t.parts[i]
		}
	}
	if !found {
		return nil, nil, errorDoesNotExists
	}
	// Open the temp file associated with the part
	partFd, err := os.OpenFile(part.name, os.O_RDONLY, 0755)
	if err != nil {
		return nil, nil, err
	}
	size := part.Size
	if part.compressedSize > size {
		size = part.compressedSize
	}
	// Read the data
	data := make([]byte, size)
	_, err = partFd.Read(data)
	if err != nil {
		return nil, nil, err
	}
	_ = partFd.Close()
	// Decompress the data
	buffer := bytes.NewBuffer(data)
	decompressor, err := zlib.NewReader(buffer)
	if err != nil {
		return nil, nil, err
	}
	err = decompressor.Close()
	if err != nil {
		return nil, nil, err
	}
	outputBuf := bytes.NewBuffer(nil)
	_, _ = io.Copy(outputBuf, decompressor)
	output := outputBuf.Bytes()
	return output, part, nil
}

// Removes a part at the index
func (t *TmpWriter) Remove(id string, index uint) error {
	t.lock.Lock()
	defer t.lock.Unlock()
	if id != t.id {
		return errorInvalidId
	}
	found := false
	part := t.parts[0]
	i := 0
	for ; i < len(t.parts); i++ {
		if t.parts[i].Index == index {
			part = t.parts[i]
			found = true
			break
		}
	}
	if !found {
		return errorDoesNotExists
	}
	err := os.Remove(part.name)
	if err != nil {
		return err
	}
	t.parts = append(t.parts[:i], t.parts[i+1:]...)
	return nil
}

// Combines all the parts into one file and deletes the temp directory
func (t *TmpWriter) CombineAndMove(dir, name string) error {
	t.lock.Lock()
	defer t.lock.Unlock()
	outputName := dir + "/" + name
	file, err := os.OpenFile(outputName, os.O_WRONLY|os.O_CREATE, 0755)
	if err != nil {
		return err
	}
	// Make sure the parts are in order based on index
	sort.Slice(t.parts, func(i, j int) bool { return t.parts[i].Index < t.parts[j].Index })
	for i := 0; i < len(t.parts); i++ {
		part := t.parts[i]
		partFd, err := os.OpenFile(part.name, os.O_RDONLY, 0755)
		if err != nil {
			_ = file.Close()
			_ = os.Remove(outputName)
			return err
		}
		size := part.Size
		if part.compressedSize > size {
			size = part.compressedSize
		}
		data := make([]byte, size)
		_, err = partFd.Read(data)
		if err != nil {
			_ = file.Close()
			_ = os.Remove(outputName)
			return err
		}
		_ = partFd.Close()
		buffer := bytes.NewBuffer(data)
		decompressor, err := zlib.NewReader(buffer)
		if err != nil {
			_ = file.Close()
			_ = os.Remove(outputName)
			return err
		}
		err = decompressor.Close()
		if err != nil {
			_ = file.Close()
			_ = os.Remove(outputName)
			return err
		}
		outputBuf := bytes.NewBuffer(nil)
		_, _ = io.Copy(outputBuf, decompressor)
		output := outputBuf.Bytes()
		h := sha256.Sum256(output)
		hash := hex.EncodeToString(h[:])
		if hash != part.Hash {
			_ = file.Close()
			_ = os.Remove(outputName)
			return errorCorrupted
		}
		_, err = file.Write(output)
		if err != nil {
			_ = file.Close()
			_ = os.Remove(outputName)
			return err
		}
	}
	err = file.Close()
	if err != nil {
		_ = os.Remove(outputName)
		return err
	}
	return nil
}

func (t *TmpWriter) GetDirName() string {
	return t.dir
}

func (t *TmpWriter) GetPartsLen() uint {
	return uint(len(t.parts))
}

// Removes all parts and deletes the folder
func (t *TmpWriter) Close() {
	t.lock.Lock()
	defer t.lock.Unlock()
	for i := 0; i < len(t.parts); i++ {
		_ = os.Remove(t.parts[i].name)
	}
	t.parts = nil
	_ = os.Remove(t.dir)
}
