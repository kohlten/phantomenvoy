package internal

import (
	"errors"
	"gitlab.com/kohlten/phantomenvoy/internal/clientBank"
	"gitlab.com/kohlten/phantomenvoy/internal/events"
	"gitlab.com/kohlten/phantomenvoy/internal/file"
	"gitlab.com/kohlten/phantomenvoy/internal/globals"
	"gitlab.com/kohlten/phantomenvoy/internal/receive"
	"gitlab.com/kohlten/phantomenvoy/internal/searcher"
	"gitlab.com/kohlten/phantomenvoy/internal/send"
)

var (
	ListenerNotRunning = errors.New("listener is not running")
	ListenerRunning = errors.New("listener is already running")
)

type Phantom struct {
	eventManager         *events.EventManager
	internalEventManager *events.EventManager
	sender               *send.Sender
	receiver             *receive.Receiver
	listener             *searcher.Listener
}

func NewPhantom() (*Phantom, error) {
	p := new(Phantom)
	p.eventManager = events.NewEventManager()
	p.internalEventManager = events.NewEventManager()
	globals.Events = p.internalEventManager
	return p, nil
}

/* Returns the next event.
If there is no event, it will return null.

This function must be called to get states.
 */
func (p *Phantom) Poll() events.Event {
	for i := uint(0); i < p.internalEventManager.Len(); {
		event := p.internalEventManager.Get(i)
		switch e := event.(type) {
		case *events.SendResult:
			p.internalEventManager.Remove(i)
			p.eventManager.Push(e)
		case *events.RequestToSend:
			p.internalEventManager.Remove(i)
			p.eventManager.Push(e)
		case *events.Error:
			p.internalEventManager.Remove(i)
			p.eventManager.Push(e)
		default:
			i++
		}
	}
	return p.eventManager.Poll()
}

func (p *Phantom) Push(event events.Event) {
	p.internalEventManager.Push(event)
}

func (p *Phantom) StartReceiver() error {
	if p.receiver != nil {
		return errors.New("receiver has already been started")
	}
	r, err := receive.NewReceiver()
	if err != nil {
		return err
	}
	p.receiver = r
	return nil
}

func (p *Phantom) StartSender() error {
	if p.sender != nil {
		return errors.New("sender has already been started")
	}
	p.sender = send.NewSender()
	return nil
}

func (p *Phantom) StopReceiver() error {
	if p.receiver == nil {
		return errors.New("receiver is not started")
	}
	p.receiver.Close()
	p.receiver = nil
	return nil
}

func (p *Phantom) StopSender() error {
	if p.sender == nil {
		return errors.New("Sender is not started")
	}
	p.sender.Close()
	p.sender = nil
	return nil
}

func (p *Phantom) StartListener() error {
	if p.listener != nil {
		return ListenerNotRunning
	}
	listener, err := searcher.NewListener()
	if err != nil {
		return err
	}
	p.listener = listener
	return nil
}

func (p *Phantom) StopListener() error {
	if p.listener == nil {
		return ListenerNotRunning
	}
	p.listener.Close()
	p.listener = nil
	return nil
}

func (p *Phantom) Search() error {
	if p.listener == nil {
		return ListenerNotRunning
	}
	return searcher.Search()
}

func (p *Phantom) GetAddresses() ([]*clientBank.Client, error) {
	if p.listener == nil {
		return nil, ListenerNotRunning
	}
	addresses := globals.Clients.GetAll()
	return addresses, nil
}

func (p *Phantom) ClearAddresses() error {
	if p.listener == nil {
		return ListenerNotRunning
	}
	globals.Clients.Clear()
	return nil
}

func (p *Phantom) Cancel(id string) {
	if p.sender != nil {
		p.sender.Cancel(id)
	}
	if p.receiver != nil {
		p.receiver.Cancel(id)
	}
}

func (p *Phantom) Send(name string, filename string) error {
	client := globals.Clients.GetIP(name)
	if client == nil {
		client = globals.Clients.GetName(name)
	}
	ip := ""
	if client != nil {
		ip = client.Ip
	} else {
		ip = name
	}
	if p.sender == nil {
		return errors.New("sender is not started")
	}
	return p.sender.Send(ip, filename)
}

func (p *Phantom) GetFile(id string) (*file.CompletedFile, error) {
	if p.receiver == nil {
		return nil, errors.New("receiver is not started")
	}
	f, err := p.receiver.GetFile(id)
	return f, err
}

func (p *Phantom) GetReceivedFileInfo() []*file.Info {
	return p.receiver.GetFinishedFileInfo()
}

func (p *Phantom) GetProcessingFileInfo() []*file.Info {
	return p.receiver.GetProcessingFileInfo()
}

func (p *Phantom) GetSendingFileInfo() []*file.Info {
	return p.sender.GetSendingFileInfo()
}

func (p *Phantom) Close() {
	if p.sender != nil {
		p.sender.Close()
	}
	if p.receiver != nil {
		p.receiver.Close()
	}
	if p.listener != nil {
		p.listener.Close()
	}
}
