<h1> PhantomEnvoy </h1>

PhantomEnvoy is a small program that allows you to transfer files from one computer to another computer on the same network.

At the moment, it works on Windows, Linux, and Mac.

To build you need a working golang install.
If you don't have golang, you can find the install steps here:
```
https://golang.org/doc/install
```
After doing that, just clone and run 'make'. This will build everything and place the binaries into the bin folder.

<h2> GUI </h2>

To run the gui, simply run ./bin/phantom_envoy_gui
After doing so, you should be greeted with this.

![gui_blank](assets/gui_blank.png)

Available will show the current computers on the local network you can send to. 
The first two should always be localhost.

On the top right of the Available tab, is Refresh. Refresh will remove the current clients and look for new ones.

On the far right, we have the modules section. This can be used to turn off the different modules. 

The Sending section shows the progress of the current files being sent. While sending it will look something like this.

![gui_sending](assets/gui.png)

The X on the right side will cancel the file currently being sent.

To send a file, you'd press "Send" on the IP you wish to send to. A menu like this should pop up.

![gui_send](assets/send_file.png)

You can hit open to select a file, or you can go to the text tab which should look like this.

![gui_text](assets/send_text.png)

Then after selecting or inputting your text, simply press "Send" at the bottom.

The receiver will see something like this.

![gui_request](assets/request.png)

The receiver can then accept or deny that file.

If the receiver accepts that file, it will then send that file. After it is done, on the receiver end, you should see a screen like this.

![gui_result](assets/result.png)

The file tab will allow you to select a folder to place it in. The text tab will give a preview of the data if it's not binary.
If you select a folder, when you press "Ok", it will place the file that was sent in that folder.

<h2> CLI </h2>

Running the CLI is pretty simple. On a terminal, run ./bin/phantom_envoy_cli. After doing that you can run help to see all of the commands.
A basic rundown to send to yourself though:

![overall](assets/overall.png)

```
    Sender:
        strl  Starts the listening module. This is used to find clients on the local network that you can connect to.
        strr  Starts the receiving module. This allows people to send data to you.
        strs  Starts the sending module. This allows you to send the data.
        s     Searches for clients.
        ac    Prints all of the available clients. There should always be two if there is no one else on the network running the program.
    
    Receiver:
        sd    Sends a request to send data to the person on the address.
        poll  Poll is used to look at events that are happening during the program. In this case, it tells us that we have an incoming file.
        rr    Is used to give a response to a request. We have to get the id of the file and send the response. True tells the other person to go ahead and send it.
        poll  Here we see poll returned that the file was sent successfully.
        gd    We then print the data using the ID.
    
    q     Exits out of the cli.
```

For the sender, these would be your commands:

![sender](assets/sender.png)

For the receiver, these would be your commands:

![receiver](assets/receiver.png)
